-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.86
-- Generation Time: 25-Ago-2016 às 19:38
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.24-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rainha_prod`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_clientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `imagem_clientes`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Banner Index 1', '2607201611081378170137.jpg', NULL, 'SIM', NULL, '1', 'banner-index-1', '/produtos', NULL, NULL),
(2, 'Banner Index 2', '2607201611081177866744.jpg', NULL, 'SIM', NULL, '1', 'banner-index-2', '', NULL, NULL),
(3, 'Banner Index 3', '2607201611081333347064.jpg', NULL, 'SIM', NULL, '1', 'banner-index-3', '', NULL, NULL),
(4, 'Banner index 4', '2607201611081264474811.jpg', NULL, 'SIM', NULL, '1', 'banner-index-4', '', NULL, NULL),
(5, 'banner produtos 1', '2706201607401182966703.jpg', NULL, 'SIM', NULL, '3', 'banner-produtos-1', '', NULL, NULL),
(6, 'Banner produtos 2', '2706201607411115025032.jpg', NULL, 'SIM', NULL, '3', 'banner-produtos-2', '', NULL, NULL),
(7, 'Banner produtos 3', '2706201607431173715614.jpg', NULL, 'SIM', NULL, '3', 'banner-produtos-3', '', NULL, NULL),
(8, 'Banner produtos 4', '2706201607471151728314.jpg', NULL, 'SIM', NULL, '3', 'banner-produtos-4', '', NULL, NULL),
(9, 'Mobile Index 01', '0208201610561335253846.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-01', '', NULL, NULL),
(10, 'Mobile index 02', '0208201610571178414145.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-02', '', NULL, NULL),
(11, 'Mobile index 03', '0208201610571335573164.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-03', '', NULL, NULL),
(12, 'Mobile index 04', '0208201610571261144784.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-04', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '0108201610211251163827.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Dicas', '0108201608511232707267.jpg', NULL, 'dicas', 'SIM', NULL),
(3, 'Dicas Dentro', '0108201608511297921053.jpg', NULL, 'dicas-dentro', 'SIM', NULL),
(4, 'Fale Conosco', '0108201608511189079355.jpg', NULL, 'fale-conosco', 'SIM', NULL),
(5, 'Trabalhe Conosco', '0108201610151207316953.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(6, 'Produtos', '3007201612231397949584.jpg', NULL, 'produtos', 'SIM', NULL),
(7, 'Produtos Dentro', '3007201603321126940142.jpg', NULL, 'produtos-dentro', 'SIM', NULL),
(8, 'Serviços', '3007201605551281918677.jpg', NULL, 'servicos', 'SIM', NULL),
(9, 'Serviços Dentro', '3007201606421234787394.jpg', NULL, 'servicos-dentro', 'SIM', NULL),
(10, 'Orçamentos', '3107201602291397748870.jpg', NULL, 'orcamentos', 'SIM', NULL),
(11, 'Mobile Empresa', '0208201604281381235586.jpg', NULL, 'mobile-empresa', 'SIM', NULL),
(12, 'Mobile Dicas', '0208201605201186305853.jpg', NULL, 'mobile-dicas', 'SIM', NULL),
(13, 'Mobile Dicas Dentro', '0208201606371189380426.jpg', NULL, 'mobile-dicas-dentro', 'SIM', NULL),
(14, 'Mobile Produtos', '0208201607251311314147.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(15, 'Mobile produtos Dentro', '0208201607261262084490.jpg', NULL, 'mobile-produtos-dentro', 'SIM', NULL),
(16, 'Mobile Serviços', '0208201607581184937349.jpg', NULL, 'mobile-servicos', 'SIM', NULL),
(17, 'Mobile Serviços Dentro', '0208201607581218666415.jpg', NULL, 'mobile-servicos-dentro', 'SIM', NULL),
(18, 'Mobile Fale conosco', '0308201609091399126498.jpg', NULL, 'mobile-fale-conosco', 'SIM', NULL),
(19, 'Mobile Trabalhe conosco', '0308201610121242149946.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', NULL),
(20, 'Mobile Orçamento', '0308201605471221321313.jpg', NULL, 'mobile-orcamento', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'correias', NULL, '3007201608041297711215.jpg', 'SIM', NULL, 'correias', '', '', ''),
(74, 'PARA SUA CASA', NULL, '3007201608041297711215.jpg', 'SIM', NULL, 'para-sua-casa', '', '', ''),
(76, 'PARA SEU ESCRITÓRIO', NULL, '3007201608041297711215.jpg', 'SIM', NULL, 'bombeamento-e-pressao', '', '', ''),
(77, 'CONSTRUÇÃO DE PISCINAS', NULL, '3007201608041297711215.jpg', 'SIM', NULL, 'construcao-de-piscinas', NULL, NULL, NULL),
(78, 'PRODUTOS PARA PISCINAS', NULL, '3007201608041297711215.jpg', 'SIM', NULL, 'produtos-para-piscinas', NULL, NULL, NULL),
(79, 'TRATAMENTO PARA PISCINAS', NULL, '3007201608041297711215.jpg', 'SIM', NULL, 'tratamento-para-piscinas', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_clientes`
--

CREATE TABLE `tb_clientes` (
  `idcliente` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '0108201604021267476397..jpg', 'SIM', NULL, 'parceiro-2', '', '', '', ''),
(3, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '0108201603581396964261..jpg', 'SIM', NULL, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '0108201603581396964261..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Av. 4ª Radial, 964, Setor Pedro Ludovico, Goiânia-GO', '1111-1111', '2222-2222', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15284.479701481092!2d-49.259768!3d-16.720867!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x663cddc6b5b9911c!2sShopping+das+Baterias+Goi%C3%A2nia!5e0!3m2!1spt-BR!2sbr!4v1467213712205', NULL, NULL, 'marciomas@gmail.com', 'https://plus.google.com/106361277453489977821', '3333-3333', '3333-4444', '(61)', '(61)', '(61', '(61)', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'Dica 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'Dica 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'Dica 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'Dica 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'Dica 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'Dica 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'Dicas Mobile Lorem ipsum dolor sit amet, consetetur', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL),
(43, 'dica teste final', '<p>\r\n	dica teste final descricao</p>', '2907201608571263252280..jpg', 'SIM', NULL, 'dica-teste-final', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'NOSSA EMPRESA', '<p>\r\n	&nbsp;empresa home-teste Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'SIM', 0, '', '', '', 'nossa-empresa', NULL, NULL, NULL),
(2, 'PERFIL DE TRABALHO', 'Serviço de atendimento Let The Waterworks Begin. There’s something particularly attractive about water. Sometimes, we feel the need to get close\rr to it wherever we see it, especially if it’s good–looking. The truth be told, water has been used throughout history as a way\r to channel out bad energy and as a medium for relaxation. A fountain or a pond are not very expensive. Creating a space t\rhat creates a seamless synergy be', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(3, 'ESTRUTURA ORGANIZACIONAL', 'Index empresa A empresa Porta de Aço Centro Oeste atua na região a mais de 25 anos. Com o objetivo de satisfazer e até mesmo suplantar as necessidades dos clientes, trabalhamos com a missão deprestar o melhor serviço, com preços justos, e qualidade incomparável.', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(4, 'ÁREA DE ATUAÇÃO', '<p>\r\n	TESTE AREA ATUACAO A maioria das instala&ccedil;&otilde;es de sistemas de pain&eacute;is solares levam apenas um &uacute;nico dia. Vamos encontrar um dia que funciona melhor para voc&ecirc;. Os coletores solares para piscinas podem ser instalados em edifica&ccedil;&otilde;es novas ou antigas, de prefer&ecirc;ncia, no n&iacute;vel mais elevado (telhado), ou ser adaptado em outro lugar da resid&ecirc;ncia. Caso o telhado n&atilde;o possa ser utilizado, h&aacute; possibilidade de construir uma base para a instala&ccedil;&atilde;o das placas solares em qualquer local de sua casa.</p>', 'SIM', 0, '', '', '', 'area-de-atuacao', NULL, NULL, NULL),
(5, 'NOSSA EQUIPE', '<p>\r\n	TESTE Eu sou parte de uma equipe. Ent&atilde;o, quando ven&ccedil;o, n&atilde;o sou eu apenas quem vence. De certa forma, termino o trabalho de um grupo enorme de pessoas!</p>\r\n<p>\r\n	Lideran&ccedil;a &eacute; o processo de conduzir um grupo de pessoas, transformando-o numa equipe que gera resultados.</p>', 'SIM', 0, '', '', '', 'nossa-equipe', NULL, NULL, NULL),
(6, 'NOSSOS SERVIÇOS', 'nosso teste Serviço de atendimento Let The Waterworks Begin. There’s something particularly attractive about water. Sometimes, we feel the need to get close\rr to it wherever we see it, especially if it’s good–looking. The truth be told, water has been used throughout history as a way\r to channel out bad energy and as a medium for relaxation. A fountain or a pond are not very expensive. Creating a space t\rhat creates a seamless synergy be', 'SIM', 0, '', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipes`
--

CREATE TABLE `tb_equipes` (
  `idequipe` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_equipes`
--

INSERT INTO `tb_equipes` (`idequipe`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `cargo`, `telefone`, `email`, `facebook`) VALUES
(1, 'Ana Lídia', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'ana-lidia', '', '', '', NULL, 'limpeza', '5555-5555', 'teste1@gmail.com', 'https://www.facebook.com/profile.php?id=100009145838170'),
(2, 'Joana Dark', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'joana-dark', '', '', '', NULL, 'Gerente de Vendas', '', '', ''),
(3, 'Daniela Alves', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'daniela-alves', '', '', '', NULL, 'Analista de mercado', '', '', ''),
(4, 'Marina Ruy', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'marina-ruy', '', '', '', NULL, 'Atriz', '', '', ''),
(5, 'Fernanda Alves', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'fernanda-alves', '', '', '', NULL, 'Modelo', '', '', ''),
(6, 'Ana Maria', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'ana-maria', '', '', '', NULL, 'Vendedora', '', '', ''),
(7, 'Gisele Almeida', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'gisele-almeida', '', '', '', NULL, 'Supervisora', '', '', ''),
(8, 'Luana Marta', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'luana-marta', '', '', '', NULL, 'Supervisora', '', '', ''),
(9, 'Ana Lídia', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'ana-lidia', '', '', '', NULL, 'serviços gerais', '', '', ''),
(10, 'Joana Dark', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'joana-dark', '', '', '', NULL, 'Gerente de Vendas', '', '', ''),
(11, 'Daniela Alves', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'daniela-alves', '', '', '', NULL, 'Analista de mercado', '', '', ''),
(12, 'Marina Ruy', '<p>\r\n	O gerente de vendas da Porta de A&ccedil;o Centro Oeste atua no mercado de vendas h&aacute; mais de 20 anos, &eacute; o profissional respons&aacute;vel por planejar, orientar, dirigir e controlar toda a &aacute;rea comercial e de vendas de nossa empresa.</p>', '1807201610151196460154..jpg', 'SIM', NULL, 'marina-ruy', '', '', '', NULL, 'Atriz', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_facebook`
--

CREATE TABLE `tb_facebook` (
  `idface` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_facebook`
--

INSERT INTO `tb_facebook` (`idface`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'face 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'face-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'face 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'face 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'face 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'face 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'face 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'face 7 Lorem ipsum dolor sit amet', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(10, '1308201504393895775783.jpg', 'SIM', NULL, NULL, 1),
(12, '1308201504399165728348.jpg', 'SIM', NULL, NULL, 1),
(25, '1308201504398260586195.jpg', 'SIM', NULL, NULL, 1),
(62, '0701201602423923561643.jpg', 'SIM', NULL, NULL, 1),
(63, '0701201602473330299303.jpg', 'SIM', NULL, NULL, 1),
(64, '0701201602556817639511.jpg', 'SIM', NULL, NULL, 264),
(65, '0701201602584890882323.jpg', 'SIM', NULL, NULL, 265),
(66, '0701201603052662793741.jpg', 'SIM', NULL, NULL, 267),
(67, '0701201603079807938151.jpg', 'SIM', NULL, NULL, 269),
(68, '0701201603468577005022.jpg', 'SIM', NULL, NULL, 270),
(69, '0701201603503761541176.jpg', 'SIM', NULL, NULL, 271),
(70, '0701201603534718772317.jpg', 'SIM', NULL, NULL, 266),
(71, '0701201603531613897755.jpg', 'SIM', NULL, NULL, 266),
(72, '0701201603557442204397.jpg', 'SIM', NULL, NULL, 272),
(73, '0701201603576661209426.jpg', 'SIM', NULL, NULL, 273),
(74, '0701201603593804380310.jpg', 'SIM', NULL, NULL, 274),
(75, '0701201604016227783949.jpg', 'SIM', NULL, NULL, 275),
(76, '0701201604041118356651.jpg', 'SIM', NULL, NULL, 276),
(77, '0701201604044298742856.jpg', 'SIM', NULL, NULL, 276),
(78, '0701201604055134400675.jpg', 'SIM', NULL, NULL, 277),
(79, '0701201604075946890529.jpg', 'SIM', NULL, NULL, 273),
(80, '0701201604113627357784.jpg', 'SIM', NULL, NULL, 279),
(81, '0701201604137711372011.jpg', 'SIM', NULL, NULL, 280),
(82, '0701201604137732442787.jpg', 'SIM', NULL, NULL, 280),
(83, '0701201604154792952169.jpg', 'SIM', NULL, NULL, 281),
(84, '0701201604155274367257.jpg', 'SIM', NULL, NULL, 281),
(85, '0701201604186279510661.jpg', 'SIM', NULL, NULL, 282),
(86, '0701201604205150143847.jpg', 'SIM', NULL, NULL, 283),
(87, '0701201604208008419883.jpg', 'SIM', NULL, NULL, 283),
(88, '0701201604217851355729.jpg', 'SIM', NULL, NULL, 284),
(89, '0701201604232479690702.jpg', 'SIM', NULL, NULL, 285);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galeria_empresa`
--

CREATE TABLE `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(9, '0306201611111753066438.jpg', 'SIM', NULL, NULL, 1),
(10, '0306201611124553853380.jpg', 'SIM', NULL, NULL, 1),
(11, '0306201611122241070551.jpg', 'SIM', NULL, NULL, 1),
(12, '0306201611127367420051.jpg', 'SIM', NULL, NULL, 1),
(13, '0306201611124911242355.jpg', 'SIM', NULL, NULL, 1),
(15, '0306201611142929856946.jpg', 'SIM', NULL, NULL, 1),
(18, '0306201611191213586110.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL, 'NAO'),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre', 'SIM'),
(3, 'Amanda', 'b362cb319b2e525dc715702edf416f10', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'43\'', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'43\'', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'44\'', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'44\'', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'45\'', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'46\'', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'45\'', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'3\'', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'NAO\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'SIM\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'5\'', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'6\'', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'2\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'1\'', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'70\'', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'73\'', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'71\'', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'72\'', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'SIM\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'45\'', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = \'45\'', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'SIM\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = \'2\'', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'5\'', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'40\'', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'42\'', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'43\'', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'44\'', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'7\'', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'8\'', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'9\'', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'10\'', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'11\'', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'13\'', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'14\'', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'41\'', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'46\'', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1),
(489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:35:23', 1),
(490, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:48:13', 1),
(491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:16', 1),
(492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:59', 1),
(493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:27:37', 1),
(494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:28:23', 1),
(495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:33:30', 1),
(496, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:20', 1),
(497, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:27', 1),
(498, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:04:09', 1),
(499, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '13:12:53', 1),
(500, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:08:56', 1),
(501, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:10:32', 1),
(502, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:19:38', 1),
(503, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'5\'', '2016-06-07', '14:20:12', 1),
(504, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'3\'', '2016-06-07', '14:20:29', 1),
(505, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'4\'', '2016-06-07', '14:20:31', 1),
(506, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:25:43', 1),
(507, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:26:57', 1),
(508, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:31:49', 1),
(509, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:58:26', 1),
(510, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:01:49', 1),
(511, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-07', '15:02:31', 1),
(512, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:02:44', 1),
(513, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:06:22', 1),
(514, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-07', '15:06:29', 1),
(515, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'2\'', '2016-06-07', '15:07:17', 1),
(516, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:08:05', 1),
(517, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'2\'', '2016-06-07', '19:19:12', 1),
(518, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '19:20:53', 1),
(519, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = \'SIM\' WHERE idbanner = \'2\'', '2016-06-07', '16:48:33', 1),
(520, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '16:57:35', 1),
(521, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:13', 1),
(522, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:59', 1),
(523, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:05:10', 3),
(524, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:08:08', 3),
(525, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '12:30:55', 1),
(526, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '15:38:33', 1),
(527, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '18:39:40', 3),
(528, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '20:42:20', 3),
(529, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:40:49', 1),
(530, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:41:59', 1),
(531, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:43:56', 1),
(532, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:47:20', 1),
(533, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:07:52', 1),
(534, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:09:32', 1),
(535, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:10:41', 1),
(536, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:05', 1),
(537, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:39', 1),
(538, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:15:18', 1),
(539, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:16:56', 1),
(540, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:17:48', 1),
(541, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:54:16', 1),
(542, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:55:13', 1),
(543, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:37:40', 1),
(544, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:43:05', 1),
(545, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:46:24', 1),
(546, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '09:11:11', 1),
(547, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '11:34:48', 1),
(548, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '12:03:24', 1),
(549, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:02:56', 0),
(550, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:39:29', 0),
(551, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:40:31', 0),
(552, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:05:34', 0),
(553, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:13:26', 0),
(554, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:15:05', 0),
(555, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '21:31:04', 0),
(556, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:12:42', 1),
(557, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:23', 1),
(558, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:54', 1),
(559, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:14:21', 1),
(560, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:09:27', 0),
(561, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:10:20', 0),
(562, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:15:20', 0),
(563, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '01:11:29', 0),
(564, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:35:50', 1),
(565, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:55:00', 0),
(566, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:55:01', 1),
(567, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:59:56', 0),
(568, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:49:36', 1),
(569, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:50:07', 1),
(570, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '11:03:55', 1),
(571, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:52:10', 1),
(572, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '13:19:01', 1),
(573, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:00:30', 1),
(574, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:41:42', 1),
(575, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:30:13', 1),
(576, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:32:16', 1),
(577, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-06', '17:07:15', 1),
(578, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:50:58', 1),
(579, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:10', 1),
(580, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:22', 1),
(581, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:30', 1),
(582, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '13:56:40', 1),
(583, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '14:02:37', 1),
(584, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '17:20:46', 1),
(585, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-13', '21:33:07', 1),
(586, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '10:53:24', 1),
(587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:37:57', 1),
(588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:42:35', 1),
(589, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:46:29', 1),
(590, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:51:04', 1),
(591, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:24:55', 1),
(592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:27:05', 1),
(593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:28:29', 1),
(594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:02', 1),
(595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:29', 1),
(596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:39', 1),
(597, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:48', 1),
(598, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:40:05', 1),
(599, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '15:47:41', 1),
(600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:53:52', 1),
(601, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:42', 1),
(602, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:59', 1),
(603, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:13', 1),
(604, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:26', 1),
(605, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:41', 1),
(606, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:53', 1),
(607, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:59:08', 1),
(608, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '16:32:46', 1),
(609, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '18:56:30', 1),
(610, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:12', 1),
(611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:31', 1),
(612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:48', 1),
(613, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:00', 1),
(614, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:22', 1),
(615, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:46', 1),
(616, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:15:06', 1),
(617, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '19:17:31', 1),
(618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:30:31', 1),
(619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '08:52:29', 1),
(620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:58:06', 1),
(621, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '19:46:29', 1),
(622, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '19:47:35', 1),
(623, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'14\'', '2016-07-15', '19:59:25', 1),
(624, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'13\'', '2016-07-15', '19:59:32', 1),
(625, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'15\'', '2016-07-15', '19:59:36', 1),
(626, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'16\'', '2016-07-15', '19:59:42', 1),
(627, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'18\'', '2016-07-15', '19:59:45', 1),
(628, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'17\'', '2016-07-15', '19:59:49', 1),
(629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '20:14:55', 1),
(630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '11:00:46', 1),
(631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:28:19', 1),
(632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:37:56', 1),
(633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:38:07', 1),
(634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:05', 1),
(635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:53', 1),
(636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:11', 1),
(637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:27', 1),
(638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:45', 1),
(639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:06', 1),
(640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:29', 1),
(641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:58', 1),
(642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:02:46', 1),
(643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:04:37', 1),
(644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:12:13', 1),
(645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:15:36', 1),
(646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:06:46', 1),
(647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:49:01', 1),
(648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:54:14', 1),
(649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:55:10', 1),
(650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:00:07', 1),
(651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:08:04', 1),
(652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:15:21', 1),
(653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:16:03', 1),
(654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:19:17', 1),
(655, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:22:55', 1),
(656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:25:29', 1),
(657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '09:35:26', 1),
(658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '18:33:00', 1),
(659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '08:47:49', 1),
(660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:01:41', 1),
(661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:30:24', 1),
(662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:46:31', 1),
(663, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:41:53', 1),
(664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:42:27', 1),
(665, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:01:08', 1),
(666, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:59:31', 1),
(667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '13:07:58', 1),
(668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '19:12:55', 1),
(669, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_facebook WHERE idface = \'43\'', '2016-07-21', '19:23:48', 1),
(670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:05', 1),
(671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:28', 1),
(672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:48', 1),
(673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:08', 1),
(674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:26', 1),
(675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:41', 1),
(676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:28', 1),
(677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:36', 1),
(678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:46', 1),
(679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:57', 1),
(680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:34:45', 1),
(681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:57:51', 1),
(682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:59:26', 1),
(683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:01:05', 1),
(684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:38', 1),
(685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:55', 1),
(686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:09', 1),
(687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:26', 1),
(688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:45', 1),
(689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:06:02', 1),
(690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '18:07:45', 1),
(691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:29:36', 1),
(692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:32:50', 1),
(693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:33:06', 1),
(694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:14', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:39', 1),
(696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-29', '08:57:37', 1),
(697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '12:23:41', 1),
(698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '15:32:54', 1),
(699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:54:41', 1),
(700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:55:28', 1),
(701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '18:42:16', 1),
(702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '19:42:53', 1),
(703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:04:57', 1),
(704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:12:02', 1),
(705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-31', '14:29:17', 1),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:28', 1),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:42', 1),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:56', 1),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:52:43', 1),
(710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:15:31', 1),
(711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:21:54', 1),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '12:04:07', 1),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '15:58:27', 1),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '16:02:46', 1),
(715, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '17:35:49', 1),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:56:30', 1),
(717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:01', 1),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:12', 1),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:23', 1),
(720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '16:28:16', 1),
(721, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '17:20:26', 1),
(722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '18:37:39', 1),
(723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:25:58', 1),
(724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:26:08', 1),
(725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:36', 1),
(726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:47', 1),
(727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '09:09:03', 1),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '10:12:40', 1),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:44:59', 1),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:52:14', 1),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:12:56', 1),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:47:19', 1),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-04', '20:15:30', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_lojas`
--

CREATE TABLE `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_noticias`
--

CREATE TABLE `tb_noticias` (
  `idnoticia` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'SELO QUALISOL Programa de Qualificação de Fornecedores de Sistemas de Aquecimento Solar', '<p style="text-align: justify;">\r\n	O QUALISOL BRASIL &eacute; o Programa de Qualifica&ccedil;&atilde;o de Fornecedores de Sistemas de Aquecimento Solar, que engloba fabricantes, revendas e instaladoras. Fruto de um conv&ecirc;nio entre a ABRAVA, o INMETRO e o PROCEL/Eletrobras, o programa tem como objetivo garantir ao consumidor qualidade dos fornecedores de sistemas de aquecimento solar, de modo a permitir:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o do conhecimento de fornecedores em rela&ccedil;&atilde;o ao aquecimento solar;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o da base de mercado do aquecimento solar e suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	O aumento da qualidade das instala&ccedil;&otilde;es e conseq&uuml;ente satisfa&ccedil;&atilde;o do consumidor final;</p>\r\n<p style="text-align: justify;">\r\n	Uma melhor e mais duradoura reputa&ccedil;&atilde;o e confian&ccedil;a em sistemas de aquecimento solar nas suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	Um crescente interesse e habilidade dos fornecedores na prospec&ccedil;&atilde;o de novos clientes e est&iacute;mulo ao surgimento de novos empreendedores.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Assim como em diversos pa&iacute;ses do mundo, no Brasil, as revendas e instaladores representam uma posi&ccedil;&atilde;o estrat&eacute;gica com rela&ccedil;&atilde;o &agrave; difus&atilde;o do aquecimento solar. Na maioria das vezes est&atilde;o em contato direto com o consumidor no momento de decis&atilde;o de compra e instala&ccedil;&atilde;o e algumas vezes tamb&eacute;m planejam e entregam os equipamentos e s&atilde;o respons&aacute;veis diretos por garantir uma instala&ccedil;&atilde;o qualificada com funcionamento, durabilidade e est&eacute;tica assegurados e comprovados. Al&eacute;m das revendas e instaladoras, as empresas fabricantes de equipamentos solares tamb&eacute;m realizam instala&ccedil;&otilde;es e contratos diretos com consumidores finais e assumem a responsabilidade por todo o processo desde a venda at&eacute; a instala&ccedil;&atilde;o e p&oacute;s-venda.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Portanto, a qualidade dos sistemas de aquecimento solar depende diretamente de:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	bons produtos, etiquetados;</p>\r\n<p style="text-align: justify;">\r\n	bons projetistas e revendas, qualificadas;</p>\r\n<p style="text-align: justify;">\r\n	bons instaladores, qualificados;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '0206201605386174124988..jpg', 'SIM', NULL, 'selo-qualisol-programa-de-qualificacao-de-fornecedores-de-sistemas-de-aquecimento-solar', '', '', '', NULL),
(50, 'ENERGIA RENOVÁVEL NO BRASIL - NÃO BASTA SOMENTE GERAR, TEMOS QUE SABER USÁ-LA', '<p>\r\n	A gera&ccedil;&atilde;o de energia renov&aacute;vel mundial apresentar&aacute; forte crescimento nos pr&oacute;ximos anos, com expectativa de crescimento de 12,7% no per&iacute;odo entre 2010 a 2013 segundo a International Energy Agency - IEA. As raz&otilde;es principais dessa previs&atilde;o s&atilde;o as metas de redu&ccedil;&atilde;o de emiss&otilde;es de CO2 e mudan&ccedil;as clim&aacute;ticas; melhorias tecnol&oacute;gicas favorecendo novas alternativas; aumento na demanda de energia; ambiente regulat&oacute;rio mais favor&aacute;vel; e incentivos governamentais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Enquanto os benef&iacute;cios de desenvolver-se uma agenda s&oacute;lida para consolida&ccedil;&atilde;o da energia renov&aacute;vel no Brasil s&atilde;o evidentes, &eacute; preciso atentar-se para os riscos associados &agrave; segrega&ccedil;&atilde;o do tema de energias renov&aacute;veis do panorama geral da agenda energ&eacute;tica no Brasil, atualmente levada pela ANP (Ag&ecirc;ncia Nacional do Petr&oacute;leo, G&aacute;s Natural e Biocombust&iacute;veis) e ANEEL (Ag&ecirc;ncia Nacional de Energia El&eacute;trica. &Eacute; preciso existir um incentivo maior &agrave;s pol&iacute;ticas p&uacute;blicas que tornem economicamente vi&aacute;veis melhorias de projetos n&atilde;o s&oacute; voltadas para reduzir os gastos com energia el&eacute;trica, aumentando a efici&ecirc;ncia energ&eacute;tica dos processos, como tamb&eacute;m que possibilitem o uso sustent&aacute;vel dos recursos naturais, diz Ricardo Antonio do Esp&iacute;rito Santo Gomes, consultor em efici&ecirc;ncia energ&eacute;tica do CTE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para Gomes &eacute; preciso pensar mais na maneira como usamos a energia, n&atilde;o somente na maneira como a geramos: N&atilde;o adianta gerarmos muita energia de forma eficiente se aqui na ponta usamos chuveiro el&eacute;trico, ao inv&eacute;s de aquecedor solar t&eacute;rmico por exemplo. Portanto, a gera&ccedil;&atilde;o distribu&iacute;da &eacute; a maneira mais eficiente de se garantir um crescimento sustent&aacute;vel para a sociedade, diminuindo perdas em transmiss&atilde;o, diminuindo o impacto ambiental de grandes centrais geradoras de energia e produzindo, localmente, as utilidades de que o cliente final precisa, como energia el&eacute;trica, vapor, &aacute;gua quente e &aacute;gua gelada, garantindo que um combust&iacute;vel se transforme ao m&aacute;ximo poss&iacute;vel, em outras formas de energia, causando menor impacto poss&iacute;vel e garantindo o n&iacute;vel de conforto exigido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele acrescenta que na pauta de pesquisas e desenvolvimento est&atilde;o os sistemas que possuem o atrativo de manterem altas taxas de efici&ecirc;ncia energ&eacute;tica, baixa emiss&atilde;o de poluentes e de CO2 e redu&ccedil;&atilde;o de custos de transmiss&atilde;o.</p>\r\n<div>\r\n	&nbsp;</div>', '0206201605489606782930..jpg', 'SIM', NULL, 'energia-renovavel-no-brasil--nao-basta-somente-gerar-temos-que-saber-usala', '', '', '', NULL),
(48, 'BANHO AQUECIDO ATRAVÉS DE AQUECIMENTO SOLAR', '<p>\r\n	Apesar de abundante, a energia solar como energia t&eacute;rmica &eacute; pouco aproveitada no Brasil, com isso o banho quente brasileiro continua, em 67% dos lares brasileiros, a utilizar o chuveiro el&eacute;trico que consome 8% da eletricidade produzida ao inv&eacute;s do aquecedor solar, segundo dados do Instituto Vitae Civillis &quot;Um Banho de Sol para o Brasil&quot;, de D&eacute;lcio Rodrigues e Roberto Matajs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esses n&uacute;meros contrastam com a capacidade que o territ&oacute;rio brasileiro tem de produzir energia solar: O potencial de gera&ccedil;&atilde;o &eacute; equivalente a 15 trilh&otilde;es de MW/h, correspondente a 50 mil vezes o consumo nacional de eletricidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O projeto Cidade Solares, parceria entre o Vitae Civilis e a DASOL/ABRAVA*, foi criado para discutir, propor e acompanhar a tramita&ccedil;&atilde;o e entrada em vigor de leis que incentivam ou obrigam o uso de sistemas solares de aquecimento de &aacute;gua nas cidades e estados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A id&eacute;ia &eacute; debater e difundir conhecimento sobre o assunto, al&eacute;m de analisar as possibilidades de implanta&ccedil;&atilde;o de projetos nas cidades. Ao ganhar o t&iacute;tulo de &quot;Solar&quot;, uma cidade servir&aacute; de exemplo para outras, com a difus&atilde;o do tema e das tecnologias na pr&aacute;tica. No site, o projeto lista v&aacute;rias iniciativas de cidades que implantaram com sucesso sistemas solares, como Freiburg- na Alemanha, Graz- na &Aacute;ustria, Portland- nos EUA e Oxford- na Inglaterra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* DASOL/ ABRAVA: Departamento Nacional de Aquecimento Solar da Associa&ccedil;&atilde;o Brasileira de Refrigera&ccedil;&atilde;o, Ar-condicionado, Ventila&ccedil;&atilde;o e Aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte: Revista Super Interessante - Editora Abril</p>', '0206201605411524312164..jpg', 'SIM', NULL, 'banho-aquecido-atraves-de-aquecimento-solar', '', '', '', NULL),
(47, 'COMO FUNCIONA UM SISTEMA DE AQUECIMENTO SOLAR DE ÁGUA? VOCÊ SABE?', '<p>\r\n	A mesma energia solar que ilumina e aquece o planeta pode ser usada para esquentar a &aacute;gua dos nossos banhos, acenderem l&acirc;mpadas ou energizar as tomadas de casa. O sol &eacute; uma fonte inesgot&aacute;vel de energia e, quando falamos em sustentabilidade, em economia de recursos e de &aacute;gua, em economia de energia e redu&ccedil;&atilde;o da emiss&atilde;o de g&aacute;s carb&ocirc;nico na atmosfera, nada mais natural do que pensarmos numa maneira mais eficiente de utiliza&ccedil;&atilde;o da energia solar. Esta energia &eacute; totalmente limpa e, principalmente no Brasil, onde temos uma enorme incid&ecirc;ncia solar, os sistemas para o aproveitamento da energia do sol s&atilde;o muito eficientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para ser utilizada, a energia solar deve ser transformada e, para isto, h&aacute; duas maneiras principais de realizar essa transforma&ccedil;&atilde;o: Os pain&eacute;is fotovoltaicos e os aquecedores solares.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os primeiros s&atilde;o respons&aacute;veis pela transforma&ccedil;&atilde;o da energia solar em energia el&eacute;trica. Com esses pain&eacute;is podemos utilizar o sol para acender as l&acirc;mpadas das nossas casas ou para ligar uma televis&atilde;o. A segunda forma &eacute; com o uso de aquecedores solares, que utiliza a energia solar para aquecer a &aacute;gua que ser&aacute; direcionada aos chuveiros ou piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como funciona um sistema de aquecimento solar?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Basicamente este sistema &eacute; composto por dois elementos: Os coletores solares (pain&eacute;is de capta&ccedil;&atilde;o, que vemos freq&uuml;entemente nos telhados das casas) e o reservat&oacute;rio de &aacute;gua quente, tamb&eacute;m chamado de boiler.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os coletores s&atilde;o formados por uma placa de vidro que isola do ambiente externo aletas de cobre ou alum&iacute;nio pintadas com tintas especiais na cor escura para que absorvam o m&aacute;ximo da radia&ccedil;&atilde;o. Ao absorver a radia&ccedil;&atilde;o, estas aletas deixam o calor passar para tubos em forma de serpentina geralmente feitos de cobre. Dentro desses tubos passa &aacute;gua, que &eacute; aquecida antes de ser levada para o reservat&oacute;rio de &aacute;gua quente. Estas placas coletoras podem ser dispostas sobre telhados e lajes e a quantidade de placas instaladas varia conforme o tamanho do reservat&oacute;rio, o n&iacute;vel de insola&ccedil;&atilde;o da regi&atilde;o e as condi&ccedil;&otilde;es de instala&ccedil;&atilde;o. No hemisf&eacute;rio sul, normalmente as placas ficam inclinadas para a dire&ccedil;&atilde;o norte para receber a maior quantidade poss&iacute;vel de radia&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios s&atilde;o cilindros de alum&iacute;nio, inox e polipropileno com isolantes t&eacute;rmicos que mant&eacute;m pelo maior tempo poss&iacute;vel a &aacute;gua aquecida. Uma caixa de &aacute;gua fria abastece o sistema para que o boiler fique sempre cheio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios devem ser instalados o mais pr&oacute;ximo poss&iacute;vel das placas coletoras (para evitar perda de efici&ecirc;ncia do sistema), de prefer&ecirc;ncia devem estar sob o telhado (para evitar a perda de calor para a atmosfera) e em n&iacute;vel um pouco elevado. Dessa forma, consegue-se o efeito chamado de termossif&atilde;o, ou seja, conforme a &aacute;gua dos coletores vai esquentando, ela torna-se menos densa e vai sendo empurrada pela &aacute;gua fria. Assim ela sobe e chega naturalmente ao boiler, sem a necessidade de bombeamento. Em casos espec&iacute;ficos, em que o reservat&oacute;rio n&atilde;o possa ser instalado acima das placas coletoras, podem-se utilizar bombas para promover a circula&ccedil;&atilde;o da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E nos dias nublados, chuvosos ou &agrave; noite?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apesar de poderem ser instalados de forma independente, os aquecedores solares normalmente trabalham com um sistema auxiliar de aquecimento da &aacute;gua. Este sistema pode ser el&eacute;trico ou a g&aacute;s, e j&aacute; vem de f&aacute;brica. Quando houver uma seq&uuml;&ecirc;ncia de dias nublados ou chuvosos em que a energia gerada pelo aquecedor solar n&atilde;o seja suficiente para esquentar toda a &aacute;gua necess&aacute;ria para o consumo di&aacute;rio, um aquecedor el&eacute;trico ou a g&aacute;s &eacute; acionado, gerando &aacute;gua quente para as pias e chuveiros. O mesmo fato acontece &agrave; noite quando n&atilde;o houver mais &aacute;gua aquecida no reservat&oacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Num pa&iacute;s tropical como o nosso, na grande maioria dos dias a &aacute;gua ser&aacute; aquecida com a energia solar e, portanto, os sistemas auxiliares ficar&atilde;o desligados a maior parte do tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena economicamente?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O custo do sistema &eacute; compensado pela economia mensal na conta de energia el&eacute;trica. O Aquecedor Solar Heliotek reduz at&eacute; 80% dos custos de energia com aquecimento e em poucos anos o sistema se paga, gerando lucro ao longo de sua vida &uacute;til? 20 anos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os aquecedores solares podem ser instalados em novas obras ou durante reformas. Ao construir ou reformar, estude esta op&ccedil;&atilde;o, que &eacute; boa para o planeta e pode ser &oacute;tima para a sua conta de energia.</p>', '0206201605446692289010..jpg', 'SIM', NULL, 'como-funciona-um-sistema-de-aquecimento-solar-de-agua-voce-sabe', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(3, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE `tb_portfolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`) VALUES
(1, 'correias  teste 01', '0408201608151216349081..jpg', '<p>\r\n	01-Fabricada em material at&oacute;xico, composta de borracha sint&eacute;tica isenta de PVC,</p>', '', '', '', 'SIM', 0, 'correias--teste-01', 75, '450 1', '', '', '', '', NULL),
(2, 'correias  teste 02', '2607201604341338346666..jpg', '<p>\r\n	02-Fabricada em material at&amp;oacute;xico, composta de borracha sint&amp;eacute;tica isenta de PVC,&lt;br /&gt;<br />\r\n	branca, que garanta um apagar suave e uniforme do grafite, protegida com cinta pl&amp;aacute;stica&lt;br /&gt;<br />\r\n	para preservar as caracter&amp;iacute;sticas do produto</p>', '', '', '', 'SIM', 0, 'borraca-teste-02', 0, '451 2', NULL, NULL, NULL, NULL, NULL),
(3, 'correias  teste 03', '2607201604341338346666..jpg', '<p>\r\n	03-Fabricada em material at&amp;oacute;xico, composta de borracha sint&amp;eacute;tica isenta de PVC,&lt;br /&gt;<br />\r\n	branca, que garanta um apagar suave e uniforme do grafite, protegida com cinta pl&amp;aacute;stica&lt;br /&gt;<br />\r\n	para preservar as caracter&amp;iacute;sticas do produto</p>', '', '', '', 'SIM', 0, 'borraca-teste-03', 0, '452 3', NULL, NULL, NULL, NULL, NULL),
(4, 'correias  teste 04', '2607201604341338346666..jpg', '<p>\r\n	04-Fabricada em material at&amp;oacute;xico, composta de borracha sint&amp;eacute;tica isenta de PVC,&lt;br /&gt;<br />\r\n	branca, que garanta um apagar suave e uniforme do grafite, protegida com cinta pl&amp;aacute;stica&lt;br /&gt;<br />\r\n	para preservar as caracter&amp;iacute;sticas do produto</p>', '', '', '', 'SIM', 0, 'borraca-teste-04', 0, '450 4', NULL, NULL, NULL, NULL, NULL),
(5, 'correias  teste 05', '2607201604341338346666..jpg', '<p>\r\n	06-Fabricada em material at&amp;oacute;xico, composta de borracha sint&amp;eacute;tica isenta de PVC,&lt;br /&gt;<br />\r\n	branca, que garanta um apagar suave e uniforme do grafite, protegida com cinta pl&amp;aacute;stica&lt;br /&gt;<br />\r\n	para preservar as caracter&amp;iacute;sticas do produto</p>', '', '', '', 'SIM', 0, 'borraca-teste-05', 0, '450 5', NULL, NULL, NULL, NULL, NULL),
(6, 'correias  teste 06', '2607201604341338346666..jpg', '', '', '', '', 'SIM', 0, '', 0, '450 6', NULL, NULL, NULL, NULL, NULL),
(7, 'correias  teste 07', '2607201604341338346666..jpg', '<p>\r\n	07-Fabricada em material at&amp;oacute;xico, composta de borracha sint&amp;eacute;tica isenta de PVC,&lt;br /&gt;<br />\r\n	branca, que garanta um apagar suave e uniforme do grafite, protegida com cinta pl&amp;aacute;stica&lt;br /&gt;<br />\r\n	para preservar as caracter&amp;iacute;sticas do produto</p>', '', '', '', 'SIM', 0, 'borraca-teste-07', 0, '450 7', NULL, NULL, NULL, NULL, NULL),
(8, 'correias  teste 08', '2607201604341338346666..jpg', '<p>\r\n	08-Fabricada em material at&amp;oacute;xico, composta de borracha sint&amp;eacute;tica isenta de PVC,&lt;br /&gt;<br />\r\n	branca, que garanta um apagar suave e uniforme do grafite, protegida com cinta pl&amp;aacute;stica&lt;br /&gt;<br />\r\n	para preservar as caracter&amp;iacute;sticas do produto</p>', '', '', '', 'SIM', 0, 'borraca-teste-08', 0, '450 8', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(3, 16, 1, 6, 4, 1987, 2013, 'SIM', 0, ''),
(4, 16, 1, 7, 5, 1987, 2013, 'SIM', 0, ''),
(5, 16, 1, 8, 6, 1987, 2013, 'SIM', 0, ''),
(6, 16, 1, 9, 7, 1987, 2013, 'SIM', 0, ''),
(7, 15, 1, 7, 5, 2000, 2016, 'SIM', 0, ''),
(8, 15, 1, 8, 6, 2000, 2016, 'SIM', 0, ''),
(10, 2, 1, 6, 3, 1990, 2016, 'SIM', 0, ''),
(11, 4, 3, 5, 2, 1994, 2004, 'SIM', 0, ''),
(12, 3, 1, 7, 5, 1980, 2016, 'SIM', 0, ''),
(13, 17, 8, 7, 5, 2009, 2013, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index', NULL, NULL, 'SIM', NULL, NULL),
(2, 'Empresa', 'Empresa', NULL, NULL, 'SIM', NULL, NULL),
(3, 'Dicas', 'Dicas', NULL, NULL, 'SIM', NULL, NULL),
(5, 'Orçamentos', 'Orçamentos', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Serviços', 'Serviços', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Produtos', 'Produtos', NULL, NULL, 'SIM', NULL, NULL),
(8, 'Fale Conosco', 'Fale Conosco', NULL, NULL, 'SIM', NULL, NULL),
(9, 'Trabalhe Conosco', 'Trabalhe Conosco', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`, `imagem_principal`) VALUES
(5, 'SERVIÇO 1', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos. Oferecemos m&atilde;o-de-obra especiliazada na constru&ccedil;&atilde;o de piscinas em fibra, de vinil ou em azulejo e pastilhas em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o. Oferecemos a voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Heliossol oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nas constru&ccedil;&otilde;es de piscinas em vinil utilizamos a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nossas piscinas de azulejo e pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'servico-1', '', '', '', 0, '', '', '0206201611176686009934.png', '2906201608151298141965.jpg'),
(6, 'SERVIÇO TITULO GRANDE 1', '<p>\r\n	Todo projeto de energia solar come&ccedil;a com uma conversa r&aacute;pida para ver se solar &eacute; ideal para voc&ecirc;. Vamos discutir o seu uso de energia e dar uma olhada em seu telhado e sua resid&ecirc;ncia para que possamos preparar um or&ccedil;amento sem compromisso personalizado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma vez que temos as medi&ccedil;&otilde;es, os nossos engenheiros especialistas ir&aacute; projetar um sistema de energia solar com base nas dimens&otilde;es da sua casa e suas necessidades de energia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A maioria das instala&ccedil;&otilde;es de sistemas de pain&eacute;is solares levam apenas um &uacute;nico dia. Vamos encontrar um dia que funciona melhor para voc&ecirc;. Os coletores solares para piscinas podem ser instalados em edifica&ccedil;&otilde;es novas ou antigas, de prefer&ecirc;ncia, no n&iacute;vel mais elevado (telhado), ou ser adaptado em outro lugar da resid&ecirc;ncia. Caso o telhado n&atilde;o possa ser utilizado, h&aacute; possibilidade de construir uma base para a instala&ccedil;&atilde;o das placas solares em qualquer local de sua casa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p>\r\n		Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n	<p>\r\n		Heliossol Energia Para a Vida!</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'servico-titulo-grande-1', '', '', '', 0, '', '', '0206201605327691842909.png', '2906201608151298141965.jpg'),
(7, 'SERVIÇO 2', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos. Oferecemos m&atilde;o-de-obra especiliazada na constru&ccedil;&atilde;o de piscinas em fibra, de vinil ou em azulejo e pastilhas em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o. Oferecemos a voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Heliossol oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nas constru&ccedil;&otilde;es de piscinas em vinil utilizamos a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nossas piscinas de azulejo e pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '2707201609321298879006..jpg', 'SIM', NULL, 'servico-2', '', '', '', 0, '', '', '0206201611176686009934.png', '2906201608151298141965.jpg'),
(8, 'SERVIÇO TITULO GRANDE 2', '<p>\r\n	Todo projeto de energia solar come&ccedil;a com uma conversa r&aacute;pida para ver se solar &eacute; ideal para voc&ecirc;. Vamos discutir o seu uso de energia e dar uma olhada em seu telhado e sua resid&ecirc;ncia para que possamos preparar um or&ccedil;amento sem compromisso personalizado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma vez que temos as medi&ccedil;&otilde;es, os nossos engenheiros especialistas ir&aacute; projetar um sistema de energia solar com base nas dimens&otilde;es da sua casa e suas necessidades de energia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A maioria das instala&ccedil;&otilde;es de sistemas de pain&eacute;is solares levam apenas um &uacute;nico dia. Vamos encontrar um dia que funciona melhor para voc&ecirc;. Os coletores solares para piscinas podem ser instalados em edifica&ccedil;&otilde;es novas ou antigas, de prefer&ecirc;ncia, no n&iacute;vel mais elevado (telhado), ou ser adaptado em outro lugar da resid&ecirc;ncia. Caso o telhado n&atilde;o possa ser utilizado, h&aacute; possibilidade de construir uma base para a instala&ccedil;&atilde;o das placas solares em qualquer local de sua casa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p>\r\n		Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n	<p>\r\n		Heliossol Energia Para a Vida!</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'servico-titulo-grande-2', '', '', '', 0, '', '', '0206201605327691842909.png', '2906201608151298141965.jpg'),
(50, 'SERVIÇO 3', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos. Oferecemos m&atilde;o-de-obra especiliazada na constru&ccedil;&atilde;o de piscinas em fibra, de vinil ou em azulejo e pastilhas em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o. Oferecemos a voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Heliossol oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nas constru&ccedil;&otilde;es de piscinas em vinil utilizamos a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nossas piscinas de azulejo e pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '2707201609331226083707..jpg', 'SIM', NULL, 'servico-3', '', '', '', 0, '', '', '0206201611176686009934.png', '2906201608151298141965.jpg'),
(51, 'SERVIÇO TITULO GRANDE 3', '<p>\r\n	Todo projeto de energia solar come&ccedil;a com uma conversa r&aacute;pida para ver se solar &eacute; ideal para voc&ecirc;. Vamos discutir o seu uso de energia e dar uma olhada em seu telhado e sua resid&ecirc;ncia para que possamos preparar um or&ccedil;amento sem compromisso personalizado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma vez que temos as medi&ccedil;&otilde;es, os nossos engenheiros especialistas ir&aacute; projetar um sistema de energia solar com base nas dimens&otilde;es da sua casa e suas necessidades de energia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A maioria das instala&ccedil;&otilde;es de sistemas de pain&eacute;is solares levam apenas um &uacute;nico dia. Vamos encontrar um dia que funciona melhor para voc&ecirc;. Os coletores solares para piscinas podem ser instalados em edifica&ccedil;&otilde;es novas ou antigas, de prefer&ecirc;ncia, no n&iacute;vel mais elevado (telhado), ou ser adaptado em outro lugar da resid&ecirc;ncia. Caso o telhado n&atilde;o possa ser utilizado, h&aacute; possibilidade de construir uma base para a instala&ccedil;&atilde;o das placas solares em qualquer local de sua casa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p>\r\n		Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n	<p>\r\n		Heliossol Energia Para a Vida!</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'servico-titulo-grande-3', '', '', '', 0, '', '', '0206201605327691842909.png', '2906201608151298141965.jpg'),
(52, 'SERVIÇO 4', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos. Oferecemos m&atilde;o-de-obra especiliazada na constru&ccedil;&atilde;o de piscinas em fibra, de vinil ou em azulejo e pastilhas em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o. Oferecemos a voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Heliossol oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nas constru&ccedil;&otilde;es de piscinas em vinil utilizamos a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nossas piscinas de azulejo e pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '2707201609341266901625..jpg', 'SIM', NULL, 'servico-4', '', '', '', 0, '', '', '0206201611176686009934.png', '2906201608151298141965.jpg'),
(53, 'SERVIÇO TITULO GRANDE 4', '<p>\r\n	Todo projeto de energia solar come&ccedil;a com uma conversa r&aacute;pida para ver se solar &eacute; ideal para voc&ecirc;. Vamos discutir o seu uso de energia e dar uma olhada em seu telhado e sua resid&ecirc;ncia para que possamos preparar um or&ccedil;amento sem compromisso personalizado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma vez que temos as medi&ccedil;&otilde;es, os nossos engenheiros especialistas ir&aacute; projetar um sistema de energia solar com base nas dimens&otilde;es da sua casa e suas necessidades de energia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A maioria das instala&ccedil;&otilde;es de sistemas de pain&eacute;is solares levam apenas um &uacute;nico dia. Vamos encontrar um dia que funciona melhor para voc&ecirc;. Os coletores solares para piscinas podem ser instalados em edifica&ccedil;&otilde;es novas ou antigas, de prefer&ecirc;ncia, no n&iacute;vel mais elevado (telhado), ou ser adaptado em outro lugar da resid&ecirc;ncia. Caso o telhado n&atilde;o possa ser utilizado, h&aacute; possibilidade de construir uma base para a instala&ccedil;&atilde;o das placas solares em qualquer local de sua casa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p>\r\n		Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n	<p>\r\n		Heliossol Energia Para a Vida!</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'servico-titulo-grande-4', '', '', '', 0, '', '', '0206201605327691842909.png', '2906201608151298141965.jpg'),
(54, 'SERVIÇO 5', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos. Oferecemos m&atilde;o-de-obra especiliazada na constru&ccedil;&atilde;o de piscinas em fibra, de vinil ou em azulejo e pastilhas em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o. Oferecemos a voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Heliossol oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nas constru&ccedil;&otilde;es de piscinas em vinil utilizamos a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nossas piscinas de azulejo e pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'servico-5', '', '', '', 0, '', '', '0206201611176686009934.png', '2906201608151298141965.jpg'),
(55, 'Manutenção Preventiva', '<p>\r\n	Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy beLet The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be&nbsp;Let The Waterworks Begin. There&rsquo;s s r to it wherever we see it, especially i to channel out bad energy and as a hat creates a seamless synergy be</p>', '2707201609341407720704..jpg', 'SIM', NULL, 'manutencao-preventiva', '', '', '', 0, '', '', NULL, '2906201608151298141965.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_produtos`
--

CREATE TABLE `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'PISCINAS DE VINIL', 77, 'SIM', NULL, 'piscinas-de-vinil'),
(3, 'PISCINAS DE FIBRA', 77, 'SIM', NULL, 'piscinas-de-fibra'),
(4, 'PISCINAS EM PASTILHAS', 77, 'SIM', NULL, 'piscinas-em-pastilhas'),
(5, 'AQUECEDORES PARA PISCINAS', 75, 'SIM', NULL, 'aquecedores-para-piscinas'),
(6, 'AQUECIMENTO RESIDENCIAL', 75, 'SIM', NULL, 'aquecimento-residencial'),
(7, 'RESERVATÓRIOS', 75, 'SIM', NULL, 'reservatorios'),
(8, 'PLACAS COLETORAS', 75, 'SIM', NULL, 'placas-coletoras'),
(9, 'BOMBAS DE CALOR', 74, 'SIM', NULL, 'bombas-de-calor'),
(10, 'CONTROLADORES', 74, 'SIM', NULL, 'controladores'),
(11, 'BOMBAS DE CIRCULAÇÃO', 76, 'SIM', NULL, 'bombas-de-circulacao'),
(12, 'SISTEMA PRESSURIZADOR', 76, 'SIM', NULL, 'sistema-pressurizador'),
(13, 'FILTROS E BOMBAS', 78, 'SIM', NULL, 'filtros-e-bombas'),
(14, 'ASPIRADORES', 78, 'SIM', NULL, 'aspiradores'),
(15, 'CLORADORES', 78, 'SIM', NULL, 'cloradores'),
(16, 'ESCOVAS', 78, 'SIM', NULL, 'escovas'),
(17, 'ILUMINAÇÃO', 78, 'SIM', NULL, 'iluminacao'),
(18, 'ESCADAS', 78, 'SIM', NULL, 'escadas'),
(19, 'PRODUTOS QUÍMICOS', 79, 'SIM', NULL, 'produtos-quimicos'),
(20, 'ULTRAVIOLETA', 79, 'SIM', NULL, 'ultravioleta');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_veiculos`
--

CREATE TABLE `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `imagem`) VALUES
(1, 'Carros', 0, 'SIM', 'carros', '2006201608061191940846.png'),
(2, 'Motos', 0, 'SIM', 'motos', '2006201608071116286885.png'),
(3, 'Caminhões', 0, 'SIM', 'caminhoes', '2006201608071262816952.png'),
(8, 'Naúticos', 0, 'SIM', 'nauticos', '2006201608081392104809.png'),
(9, 'Nobreaks', 0, 'SIM', 'nobreaks', '2006201608091390486381.png'),
(10, 'Lâmpadas', 0, 'SIM', 'lampadas', '2006201608101362518866.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  ADD PRIMARY KEY (`idequipe`);

--
-- Indexes for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Indexes for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  ADD PRIMARY KEY (`idface`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  ADD PRIMARY KEY (`idgaleriaempresa`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Indexes for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  ADD PRIMARY KEY (`idprodutosmodeloaceito`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Indexes for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  ADD PRIMARY KEY (`idsubcategoriaproduto`);

--
-- Indexes for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  ADD PRIMARY KEY (`idtipoveiculo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  MODIFY `idequipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  MODIFY `idface` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  MODIFY `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=734;
--
-- AUTO_INCREMENT for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  MODIFY `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  MODIFY `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  MODIFY `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
