<div class="top50"></div>
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container top20 bottom20">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12 top20">
					<div class="barra_branca">
						<ul class="menu_rodape">
							<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
							</li>
							<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
							</li>
							<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or  Url::getURL( 0 ) == "marca"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
							</li>

							<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or  Url::getURL( 0 ) == "marca"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/servicos">SERVICOS</a>
							</li>

							<li  class="<?php if(Url::getURL( 0 ) == "noticias" or Url::getURL( 0 ) == "noticia" ){ echo "active"; } ?>">
								<a href="<?php echo Util::caminho_projeto() ?>/noticias">NOTÍCIAS</a>
							</li>


							<li  class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
								<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
							</li>

							<li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
								<a  href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
							</li>
						</ul>

					</div>
					<div class="clearfix">	</div>
				</div>
				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->


				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-xs-2 top15">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->

				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="col-xs-8 top30 endereco_rodape">
					<p class="bottom15"><i class="fa fa-home right10"></i><?php Util::imprime($config[endereco]); ?></p>
					<div class="telefone_rodape">
						<p class="bottom15">
							<i class="fa fa-phone right10"></i>
							<span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>

							<b class="left15"></b>

							<?php if (!empty($config[telefone2])) { ?>
								<span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
								<?php } ?>

								<?php if (!empty($config[telefone3])) { ?>
									<span><?php Util::imprime($config[ddd3]); ?></span><?php Util::imprime($config[telefone3]); ?>
									<?php } ?>

									<?php if (!empty($config[telefone4])) { ?>
										<span><?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?>
										<?php } ?>
									</p>

								</div>
							</div>
							<!-- ======================================================================= -->
							<!-- ENDERECO E TELEFONES    -->
							<!-- ======================================================================= -->


							<div class="col-xs-2 padding0 text-right top45">
								<?php if ($config[google_plus] != "") { ?>
									<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
										<i class="fa fa-google-plus right15"></i>
									</a>
									<?php } ?>
								<a href="http://www.homewebbrasil.com.br" target="_blank">
									<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
								</a>
							</div>





						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid">
				<div class="row rodape-preto">
					<div class="col-xs-12 text-center top10 bottom10">
						<h5>© Copyright DISKLIMP</h5>
					</div>
				</div>
			</div>
