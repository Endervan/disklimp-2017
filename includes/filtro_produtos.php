
<div class="filtro_produtos">

  <!--  ==============================================================  -->
  <!-- BARRA CATEGORIA ORDENA POR-->
  <!--  ==============================================================  -->
  <div class="col-xs-4 div_personalizada">

    <div class="dropdown">
      <a class="btn btn_filtro_marcas" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        FILTRAR POR CATEGORIAS <i class="fa fa-angle-double-down left20" aria-hidden="true"></i>
      </a>


      <div class="dropdown-menu ordenar_por pb20" aria-labelledby="dropdownMenu1">
        <div class="list-group">
            <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/produtos/" title="TODAS AS MARCAS">
                  TODAS AS CATEGORIAS
            </a>

          <?php

          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            $i = 0;
            while($row = mysql_fetch_array($result)){

              ?>

              <a class="list-group-item"
                href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?>
              </a>

                <?php

                if ($i == 1) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                }else{
                  $i++;
                }

              }
            }
            ?>

        </div>

      </div>
    </div>

  </div>
  <!--  ==============================================================  -->
  <!-- BARRA CATEGORIA ORDENA POR-->
  <!--  ==============================================================  -->




  <div class="col-xs-5">
    <!--  ==============================================================  -->
    <!--BARRA PESQUISAS-->
    <!--  ==============================================================  -->
    <div class="input-group stylish-input-group">
      <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

        <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" >
        <span class="input-group-addon">
          <button type="submit">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>

      </form>

    </div>
    <!--  ==============================================================  -->
    <!--BARRA PESQUISAS-->
    <!--  ==============================================================  -->
  </div>


</div>

<div class="clearfix"></div>
