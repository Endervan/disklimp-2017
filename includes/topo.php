
<div class="container">
  <div class="row">

    <div class="col-xs-2 logo top10">
      <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
      </a>
    </div>



    <div class="col-xs-10 padding0">
      <!-- ======================================================================= -->
      <!-- ENDERECO E TELEFONES    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top15 endereco_topo">
        <div class="telefone_rodape pull-right">
          <p>
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone_topo.png" alt="" class="right10">
            <span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>

            <b class="left15"></b>

            <?php if (!empty($config[telefone2])) { ?>
              <span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
              <?php } ?>


            </p>

          </div>

          <p class=" pull-right top10 right30">
            <img class="right10" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao_topo.png" alt="" /><?php Util::imprime($config[endereco]); ?>
          </p>

        </div>
        <!-- ======================================================================= -->
        <!-- ENDERECO E TELEFONES    -->
        <!-- ======================================================================= -->




        <!-- ======================================================================= -->
        <!-- BARRA E CATEGORIAS    -->
        <!-- ======================================================================= -->
        <div class="col-xs-8 top15 barra_topo">


          <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <div class="pull-right ">
              <div class="input-group stylish-input-group">
                <span class="input-group-addon">

                  <button type="submit"><span class="fa fa-search"></span></button>

                </span>
              </div>
            </div>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->

            <!--  ==============================================================  -->
            <!-- BARRA CATEGORIA -->
            <!--  ==============================================================  -->
            <div class="pull-right cat_topo">

              <div class="">
                <select name="select_categorias" class="selectpicker submit-select" data-live-search="true" title="CATEGORIAS" data-width="100%">
                  <optgroup label="SELECIONE">
                    <?php
                    $result = $obj_site->select("tb_categorias_produtos", "and imagem <> ''");
                    if(mysql_num_rows($result) > 0){
                      while ($row = mysql_fetch_array($result)) {
                        ?>
                        <option value="<?php Util::imprime($row[0]); ?>">
                          <div class="media">
                            <div class="media-left media-middle">
                              <img width="34" height="20" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>"  >

                            </div>
                            <div class="media-body">
                              <h1 class="media-heading"><?php Util::imprime($row[titulo]); ?></h1>
                            </div>
                          </div>
                        </option>

                        <?php
                      }
                    }
                    ?>
                  </optgroup>
                </select>
              </div>

            </div>
            <!--  ==============================================================  -->
            <!-- BARRA CATEGORIA -->
            <!--  ==============================================================  -->

            <div class="pull-right">
              <div class=" input-group stylish-input-group">
                <input type="text" class="form-control" name="busca_produtos" placeholder="O QUE VOCÊ ESTÁ PROCURANDO" >
              </div>
            </div>

          </form>
        </div>
        <!-- ======================================================================= -->
        <!-- BARRA E CATEGORIAS    -->
        <!-- ======================================================================= -->

        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-xs-4 text-right">
          <div class="dropdown">
            <a class="btn_topo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_ocamento.png" alt="" />
              <!-- <span class="badge"><?php //echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?></span> -->
            </a>

            <div class="dropdown-menu topo-meu-orcamento " aria-labelledby="dropdownMenu1">
              <!--  ==============================================================  -->
              <!--sessao adicionar produtos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php if(!empty($row[imagem])): ?>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      <?php endif; ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                  </div>
                  <?php
                }
              }
              ?>


              <!--  ==============================================================  -->
              <!--sessao adicionar servicos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_servicos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php if(!empty($row[imagem])): ?>
                        <img class="carrinho_servcos" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                        <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
                      <?php endif; ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                    </div>
                    <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                  </div>
                  <?php
                }
              }
              ?>

              <div class="col-xs-12 text-right top10 bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_topo" >
                  ENVIAR ORÇAMENTO
                </a>
              </div>
            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!--CARRINHO-->
        <!--  ==============================================================  -->
      </div>
    </div>
  </div>



  <div class="container-fluid">
    <div class="row topo">
      <div class="container">
        <div class="row">
          <!-- ======================================================================= -->
          <!-- NOSSAS CATEGORIAS    -->
          <!-- ======================================================================= -->
          <div class="col-xs-3">

            <div class="dropdown">
              <a class="btn_topo" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_categorias_topo.png" alt="" />
              </a>


              <div class="dropdown-menu topo-meu-cat pb20" aria-labelledby="dropdownMenu1">
                <div class="list-group">
                  <?php
                  $result = $obj_site->select("tb_categorias_produtos", "and imagem <> ''");
                  if(mysql_num_rows($result) > 0){
                    while ($row = mysql_fetch_array($result)) {
                      ?>
                      <div class="col-xs-12 padding0">
                        <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                          <div class="media">
                            <div class="media-left media-middle">
                              <img width="34" height="20" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>"  >

                            </div>
                            <div class="media-body">
                              <h1 class="media-heading"><?php Util::imprime($row[titulo]); ?></h1>
                            </div>
                          </div>
                        </a>
                      </div>
                      <?php
                    }
                  }
                  ?>

                </div>

              </div>
            </div>

          </div>
          <!-- ======================================================================= -->
          <!-- NOSSAS CATEGORIAS    -->
          <!-- ======================================================================= -->


          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->
          <div class="col-xs-9 menu">
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
              <ul class="nav navbar-nav navbar-right">
                <li >
                  <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/">HOME
                  </a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA
                  </a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "marca"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
                  </a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "marca"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                  </a>
                </li>



                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "noticias" or Url::getURL( 0 ) == "noticia"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/noticias">NOTÍCIAS
                  </a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
                  </a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO
                  </a>
                </li>

              </ul>

            </div><!--/.nav-collapse -->

          </div>
          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->

        </div>
      </div>

    </div>
  </div>
