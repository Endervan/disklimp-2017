<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  176px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_branco left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a class="active">PRODUTOS</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- TITULO  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top10 ">
      <div class="col-xs-12  ">
        <div class="noticias_geral">
          <h1>CONHEÇA NOSSOS <span>  PRODUTOS</span></h1>
        </div>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- TITULO  -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row  ">
      <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
        <!--  ==============================================================  -->
        <!--  MENU lateral-->
        <!--  ==============================================================  -->
        <div class="">
          <?php require_once("./includes/menu_produtos.php") ?>
        </div>
        <!--  ==============================================================  -->
        <!--  MENU lateral-->
        <!--  ==============================================================  -->
        <div class="col-xs-9 padding0">

          <!--  ==============================================================  -->
          <!--  filtro produtos-->
          <!--  ==============================================================  -->
          <div class="top50">

              <?php require_once("./includes/filtro_produtos.php") ?>

          </div>
          <!--  ==============================================================  -->
          <!--  filtro produtos-->
          <!--  ==============================================================  -->

          <!--  ==============================================================  -->
          <!--PRODUTOS HOME-->
          <!--  ==============================================================  -->

          <div class="">

            <?php

            //  FILTRA PELA MARCA
            $url1 = Url::getURL(1);
            $url2 = Url::getURL(2);


            //  FILTRA AS CATEGORIAS
            if (isset( $url1 )) {
              $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
              $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
            }


            //  FILTRO PELAS CATEGORIAS VIA POST
            if(isset($_POST[select_categorias]) and !empty($_POST[select_categorias]) ):
              $complemento .= "AND id_categoriaproduto = '$_POST[select_categorias]' ";
            endif;


            //  FILTRA PELO TITULO
            if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
              $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
            endif;



            $result = $obj_site->select("tb_produtos", $complemento);
            ?>



            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/lista_produtos.php') ?>
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->


          </div>


        </div>

      </form>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
