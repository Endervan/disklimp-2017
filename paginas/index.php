<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- BOLETO,CARTAO,SERVICOS,ATENTMENTO    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top40">
      <div class="col-xs-3">
        <div class="fundo_cinza">
          <div class="media  pb5">
            <a>
            <div class="media-left media-middle">

              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_boleto.png" alt="">

            </div>
            <div class="media-body ">
              <h2 class="media-heading ">10% OFF <span class="clearfix">NO BOLETO</span></h2>
            </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="fundo_cinza">
          <div class="media pb5">
            <a>
            <div class="media-left media-middle">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_cartao.png" alt="">

            </div>
            <div class="media-body">
              <h2 class="media-heading">EM ATE 5X <span class="clearfix">NO CARTÃO</span></h2>
            </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="fundo_cinza">
          <div class="media pb5">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
            <div class="media-left media-middle">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_servicos.png" alt="">
            </div>
            <div class="media-body">
              <h2 class="media-heading">CONFIRA NOSSOS<span class="clearfix">SERVIÇOS</span></h2>
            </div>
          </a>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="fundo_cinza">
          <div class="media pb5">
            <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
            <div class="media-left media-middle">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_atendimento.png" alt="">
            </div>
            <div class="media-body">
              <h2 class="media-heading">TIRE SUAS DÚVIDAS<span class="clearfix">ATENDIMENTO</span></h2>
            </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BOLETO,CARTAO,SERVICOS,ATENTMENTO    -->
  <!-- ======================================================================= -->

  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row top55">
      <div class="col-xs-12">
        <div class="lista-produtos-index">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs bottom25" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">EM DESTAQUES</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">OS MAIS VENDIDOS</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">OS MAIS VISITADOS</a></li>
            <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-preto">VER TODOS OS PRODUTOS</a></li>
            <div>

              <!--  ==============================================================  -->
              <!--BARRA PESQUISAS-->
              <!--  ==============================================================  -->
              <div class="input-group stylish-input-group">
                <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

                  <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" >
                  <span class="input-group-addon">
                    <button type="submit">
                      <span class="fa fa-search"></span>
                    </button>
                  </span>
                </form>
              </div>
              <!--  ==============================================================  -->
              <!--BARRA PESQUISAS-->
              <!--  ==============================================================  -->

            </div>
          </ul>
        </div>
      </div>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">

          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->


        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home01.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->

        </div>


        <div role="tabpanel" class="tab-pane" id="messages">
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home02.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->

        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->




  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_servicos">
    <div class="row">
      <div class="container">
        <div class="row ">



          <div class="col-xs-8 padding0 ">


            <div class="clearfix">  </div>
            <div class="top150">

              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
              <div class="servico_desc right20">
                <p><?php Util::imprime($row[descricao]); ?></span></p>
              </div>

              <div class="col-xs-8 padding0">
                <a href="<?php echo Util::caminho_projeto() ?>/empresa" class="btn btn_empresa_home col-xs-12" title="empresa">
                  CONHEÇA MAIS NOSSA HISTÓRIA
                </a>
              </div>
            </div>


            <div class="col-xs-12 padding0 top30 dicas_titulo">
              <h1 class="pb20">NOSSAS <span class="clearfix">NOTÍCIAS</span></h1>
            </div>


          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- nossas noticias -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top40">

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 3");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-4 top15 dicas_geral">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="caption">
              <h1 class="dicas_desc"><span><?php Util::imprime($row[titulo]); ?></span></h1>
              <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas top30" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right left10" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>


    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas noticias -->
  <!-- ======================================================================= -->

<div class="bottom40">  </div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
