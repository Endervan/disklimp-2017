<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  176px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_preto left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a class="active">A EMPRESA</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <!-- ======================================================================= -->
      <!-- conheca nossa empresa  -->
      <!-- ======================================================================= -->
      <div class="col-xs-7 empresa_geral">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
        <div class="top30">
          <h3>CONHEÇA NOSSA <span>HISTÓRIA</span></h3>
        </div>
        <div class="top35 desc_empresa_missao">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
        <!-- ======================================================================= -->
        <!-- conheca nossa empresa  -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- ATENTIMENTO  -->
        <!-- ======================================================================= -->
        <div class=" empresa_titulo1 top30">
          <h3><span>ATENDIMENTO</span></h3>
        </div>
        <!-- ======================================================================= -->
        <!-- ATENTIMENTO  -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- ENDERECO E TELEFONES    -->
        <!-- ======================================================================= -->
        <div class="top35 endereco_empresa">
          <div class="telefone_rodape">
            <p>
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone.png" alt="" class="right10">
              <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>

              <b class="left15"></b>

              <?php if (!empty($config[telefone2])) { ?>
                <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_telefone.png" alt="" class="right10">
                <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                <?php } ?>


              </p>

            </div>

            <div class=" empresa_titulo1 top30">
              <h3><span>ONDE ESTAMOS</span></h3>
            </div>


            <div class="media top35">
              <div class="media-left media-middle">
                <img class="right20" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

              </div>
              <div class="media-body ">
                <p class="media-heading right60"><?php Util::imprime($config[endereco]); ?></p>
              </div>
            </div>



          </div>
          <!-- ======================================================================= -->
          <!-- ENDERECO E TELEFONES    -->
          <!-- ======================================================================= -->

          <div class="bottom40"></div>
        </div>
      </div>
    </div>


    <div class='container '>
      <div class="row">

        <div class="col-xs-12 padding0 top80">
          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
          <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

          <!-- ======================================================================= -->
          <!-- mapa   -->
          <!-- ======================================================================= -->
        </div>
      </div>
    </div>


    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>
