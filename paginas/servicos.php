<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  176px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_preto left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a class="active">SERVIÇOS</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- nossas SERVICOS -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top20 ">


      <div class="col-xs-12  ">
        <div class="empresa_geral">
          <h3 class="pb20 ">CONFIRA NOSSOS <span>  SERVIÇOS</span></h3>
        </div>
      </div>


      <div class="col-xs-8 padding0 top50">
        <?php

        $result = $obj_site->select("tb_servicos");
        if (mysql_num_rows($result) > 0) {
          $n=0;
          while($row = mysql_fetch_array($result)){
            //  busca a qtd de produtos cadastrados
            // $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


            switch ($i) {
              case 1:
              $cor_class = 'DIREITO_TRIBUTÁRIO';
              break;
              case 2:
              $cor_class = 'DIREITO_CIVIL';
              break;
              case 3:
              $cor_class = 'DIREIT_ DO_TRABALHO';
              break;
              case 4:
              $cor_class = 'DIREITO_TRIBUTÁRIO';
              break;
              case 5:
              $cor_class = 'DIREITO_CIVIL';
              break;

              default:
              $cor_class = 'DIREITO_DO_TRABALHO';
              break;
            }
            $i++;
            ?>
            <!-- ======================================================================= -->
            <!--ITEM 01 REPETI ATE 6 ITENS   -->
            <!-- ======================================================================= -->
            <div class="col-xs-6  bg_branco servicos pt15 pb15 <?php echo $cor_class; ?> ">

              <div class="media">
                <a class="media-left media-middle" href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                </a>
                <div class="media-body">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

                    <h1 class="media-heading servico_titulo"><?php Util::imprime($row[titulo]); ?></h1>
                    <div class=" servicos_desc">
                      <p><?php Util::imprime($row[descricao]); ?></p>
                    </div>
                  </a>
                </div>

              </div>

            </div>

            <?php
            if($n == 1){
              echo '<div class="clearfix"></div>';
              $n = 0;
            }else{
              $n++;
            }

          }
        }
        ?>


      </div>
    </div>
  </div>
 <div class="clearfix">  </div>
  <!-- ======================================================================= -->
  <!-- nossas SERVICOS -->
  <!-- ======================================================================= -->

<div class="bottom120"></div>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
