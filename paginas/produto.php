<?php

// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  176px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_branco left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a href="<?php echo Util::caminho_projeto(); ?>/produtos">PRODUTO</a>
          <a class="active">  <?php Util::imprime( Util::troca_value_nome($dados_dentro[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ); ?></a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row top20 produtos_dentro">




      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->
      <div class="col-xs-5 slider_prod_geral">

        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider">
          <ul class="slides slider-prod">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result)){
                ?>
                <li class="zoom">
                  <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4" title="<?php Util::imprime($dados_dentro[titulo]); ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" />
                  </a>
                </li>
                <?php
              }
            }
            ?>

            <!-- items mirrored twice, total of 12 -->
          </ul>


        </div>
        <div id="carousel" class="flexslider">
          <i class="fa fa-search" aria-hidden="true">  Click na imagem para ver o zoom</i>

          <ul class="slides slider-prod-tumb">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result)){
                ?>
                <li>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />

                </li>
                <?php
              }
            }
            ?>
            <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>


      </div>
      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- DESCRICAO TIPOS PRODUTOS  -->
      <!-- ======================================================================= -->
      <div class="col-xs-7 padding0 descricao_tipo_produto">

        <div class="col-xs-12">
          <h1 class="text-uppercase"><?php Util::imprime($dados_dentro[titulo]); ?></h1>

          <div class="clearfix"> </div>

          <div class="top15">
            <a href="javascript:void(0);" class="" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
              <img  src="<?php echo Util::caminho_projeto() ?>/imgs/btn_produto_dentro.png"/>
            </a>
          </div>
        </div>

        <div class=" col-xs-12 padding0">
          <div class="top20 col-xs-6">
            <a class=" btn_ligue" >
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/central.png" alt="">
            </a>
          </div>

          <!-- ======================================================================= -->
          <!-- ENDERECO E TELEFONES    -->
          <!-- ======================================================================= -->
          <div class="col-xs-6 padding0 top25 telefones">
            <div class="telefone_rodape">
              <p>
                <span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>
                <b class="left5"></b>
                <?php if (!empty($config[telefone2])) { ?>
                  <span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
                <?php } ?>
              </p>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- ENDERECO E TELEFONES    -->
          <!-- ======================================================================= -->
        </div>


        <div class="col-xs-12 descricao_prod top70">
          <h2><i class="btn_prod">MARCA :</i> <span class="left10"><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_tipoveiculo], "tb_tipos_veiculos", "idtipoveiculo", "titulo") ); ?></span></h2>

          <?php /*
          <div class="top15">
          <h3 class="preco"><span>R$ </span> <?php echo Util::formata_moeda($dados_dentro[preco]); ?></h3>
          </div>
          */ ?>
        </div>

      </div>
      <!-- ======================================================================= -->
      <!-- DESCRICAO TIPOS PRODUTOS  -->
      <!-- ======================================================================= -->

    </div>
  </div>




  <div class="container">
    <div class="row produtos_dentro_desc">
      <!-- ======================================================================= -->
      <!-- DESCRICAO PRODUTOS  -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 bottom20 top20 barra_produto ">
        <h3 class="pb20"> <span>DESCRIÇÃO </span></h3>
      </div>

      <div class="col-xs-12 top10 bottom40">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>

      </div>
      <!-- ======================================================================= -->
      <!-- DESCRICAO PRODUTOS  -->
      <!-- ======================================================================= -->


    </div>
  </div>



  <!--  ==============================================================  -->
  <!--  VEJA TAMBEM-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">

      <div class="col-xs-12 bottom20 barra_produto ">
        <h3 class="pb20"> <span>VEJA TAMBÉM </span></h3>
      </div>

      <?php
        $i=0;
      $result = $obj_site->select("tb_produtos", " order by rand() limit 4");
      if(mysql_num_rows($result) > 0){

        while ($row = mysql_fetch_array($result))
        {
          $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
          $row_categoria = mysql_fetch_array($result_categoria);
          ?>

          <div class="col-xs-3">
            <div class="lista-produto">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 260, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>

              <?php /*
              <div class="text-right">
              <h3 class="preco"><span>R$ </span> <?php echo Util::formata_moeda($row[preco]); ?></h3>
              </div>
              */ ?>
              <h2 class="text-uppercase"><?php Util::imprime($row_categoria[titulo]); ?></h2>
              <h1 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h1>

              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="pull-left btn-carrinho-saiba-mais">SAIBA MAIS</a>
              <a href="javascript:void(0);" title="Adicionar ao orçamento"  data-toggle="tooltip" data-placement="top" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" class="pull-right btn-vermelho btn-carrinho">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_orcamento.png" alt="">
              </a>
              <div class="clearfix"></div>
            </div>
          </div>
          <?php if ($i == 3) {
            echo'<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  VEJA TAMBEM-->
  <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" href="http://www.elevateweb.co.uk/wp-content/themes/radial/jquery.fancybox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://www.elevateweb.co.uk/wp-content/themes/radial/jquery.elevatezoom.min.js" type="text/javascript"></script>
<script src="http://www.elevateweb.co.uk/wp-content/themes/radial/jquery.fancybox.pack.js" type="text/javascript"></script>


<script>
//initiate the plugin and pass the id of the div containing gallery images
$("#img_05").elevateZoom({
  constrainType:"height",
  constrainSize:320,
  zoomType: "inner",
  containLensZoom: true,
  gallery:'gal1',
  cursor: 'crosshair',
  galleryActiveClass: "active",
  imageCrossfade: true
});



</script>


<script type="text/javascript">
$(document).ready(function() {
  $("#carousel-gallery").touchCarousel({
    itemsPerPage: 1,
    scrollbar: true,
    scrollbarAutoHide: true,
    scrollbarTheme: "dark",
    pagingNav: false,
    snapToItems: true,
    scrollToLast: false,
    useWebkit3d: true,
    loopItems: true,
    autoplay: true
  });
});
</script>




<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->



<script>
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});


</script>
