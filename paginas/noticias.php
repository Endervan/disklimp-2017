<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  176px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_branco left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a class="active">NOTÍCIAS</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- nossas noticias -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top10 ">


      <div class="col-xs-12  ">
        <div class="noticias_geral">
        <h1>CONFIRA NOSSAS <span>  NOTÍCIAS</span></h1>
        </div>
      </div>


      <?php
      $result = $obj_site->select("tb_dicas");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="col-xs-4 top40 dicas_geral">
            <div class="thumbnail">
              <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption">
                <h1 class="dicas_desc"><span><?php Util::imprime($row[titulo]); ?></span></h1>
                <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas top30" role="button">
                  LEIA MAIS <i class="fa fa-long-arrow-right left10" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <?php
          }
        }
        ?>


      </div>
    </div>

    <!-- ======================================================================= -->
    <!-- nossas noticias -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>
