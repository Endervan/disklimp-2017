<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  176px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12 breadcrumbs_preto left15">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
          <a class="active">FALE CONOSCO</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- fale conosco -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top20 ">
      <div class="col-xs-12  ">
        <div class="empresa_geral">
          <h3 >SEMPRE QUE PRECISAR <span>  FALE CONOSCO</span></h3>
        </div>
      </div>

      <!-- ======================================================================= -->
      <!-- ENDERECO E TELEFONES    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top15 endereco_topo">
        <div class="telefone_rodape">
          <p>
            <span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>

            <b class="left15"></b>

            <?php if (!empty($config[telefone2])) { ?>
              <span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
              <?php } ?>

              <?php if (!empty($config[telefone3])) { ?>
                <span><?php Util::imprime($config[ddd3]); ?></span><?php Util::imprime($config[telefone3]); ?>
                <?php } ?>

                <?php if (!empty($config[telefone4])) { ?>
                  <span><?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?>
                  <?php } ?>
                </p>
              </div>

              <div class="empresa_geral top40">
                <h3 >ENVIE UM<span> EMAIL</span></h3>
              </div>


            </div>
            <!-- ======================================================================= -->
            <!-- ENDERECO E TELEFONES    -->
            <!-- ======================================================================= -->


            <!--  ==============================================================  -->
            <!-- FORMULARIO-->
            <!--  ==============================================================  -->
            <div class="col-xs-7 top20">
              <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
                <div class="fundo_formulario">
                  <!-- formulario orcamento -->
                  <div class="top20">
                    <div class="col-xs-6">
                      <div class="form-group input100 has-feedback">
                        <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                        <span class="fa fa-user form-control-feedback top15"></span>
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                        <span class="fa fa-envelope form-control-feedback top15"></span>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>


                  <div class="top20">
                    <div class="col-xs-6">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                        <span class="fa fa-phone form-control-feedback top15"></span>
                      </div>
                    </div>

                    <div class="col-xs-6">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                        <span class="glyphicon glyphicon-star form-control-feedback top5"></span>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="top20">
                    <div class="col-xs-12">
                      <div class="form-group  input100 has-feedback">
                        <input type="text" name="fala" class="form-control fundo-form1 input-lg input100" placeholder="FALAR COM">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="top15">
                    <div class="col-xs-12">
                      <div class="form-group input100 has-feedback">
                        <textarea name="mensagem" cols="25" rows="10" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                        <span class="fa fa-pencil form-control-feedback top15"></span>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-6 top30">

                  </div>
                  <div class="col-xs-6 text-right">
                    <div class="top15 bottom25">
                      <button type="submit" class="btn btn_formulario1" name="btn_contato">
                        ENVIAR
                      </button>
                    </div>
                  </div>

                </div>
              </form>
            </div>
            <!--  ==============================================================  -->
            <!-- FORMULARIO-->
            <!--  ==============================================================  -->
          <div class="clearfix"></div>

            <div class="col-xs-6">
              <div class=" empresa_titulo1 top30">
                <h3><span>ONDE ESTAMOS</span></h3>
              </div>


              <div class="media top35">
                <div class="media-left media-middle">
                  <img class="right20" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

                </div>
                <div class="media-body ">
                  <p class="media-heading right60"><?php Util::imprime($config[endereco]); ?></p>
                </div>
              </div>
            </div>



          </div>
        </div>

        <!-- ======================================================================= -->
        <!-- fale conosco -->
        <!-- ======================================================================= -->


        <div class='container '>
          <div class="row">

            <div class="col-xs-12 padding0 top170">
              <!-- ======================================================================= -->
              <!-- mapa   -->
              <!-- ======================================================================= -->
              <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

              <!-- ======================================================================= -->
              <!-- mapa   -->
              <!-- ======================================================================= -->
            </div>
          </div>
        </div>

        <!-- ======================================================================= -->
        <!-- rodape    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/rodape.php') ?>
        <!-- ======================================================================= -->
        <!-- rodape    -->
        <!-- ======================================================================= -->



      </body>

      </html>


      <?php require_once('./includes/js_css.php') ?>




      <?php
      //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_POST[nome]))
                          {
                            $texto_mensagem = "
                            Nome: ".($_POST[nome])." <br />
                            Email: ".($_POST[email])." <br />
                            Telefone: ".($_POST[telefone])." <br />
                            Assunto: ".($_POST[assunto])." <br />
                            Fala com: ".($_POST[fala])." <br />

                            Mensagem: <br />
                            ".(nl2br($_POST[mensagem]))."
                            ";



                            if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
                              Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
                              Util::alert_bootstrap("Obrigado por entrar em contato.");
                              unset($_POST);
                            }else{
                              Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
                            }

                        }


      ?>



      <script>
      $(document).ready(function() {
        $('.FormContatos').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-remove',
            validating: 'fa fa-refresh'
          },
          fields: {
            nome: {
              validators: {
                notEmpty: {
                  message: 'Insira seu nome.'
                }
              }
            },
            mensagem: {
              validators: {
                notEmpty: {
                  message: 'Insira sua Mensagem.'
                }
              }
            },
            email: {
              validators: {
                notEmpty: {
                  message: 'Informe um email.'
                },
                emailAddress: {
                  message: 'Esse endereço de email não é válido'
                }
              }
            },
            telefone: {
              validators: {
                notEmpty: {
                  message: 'Por favor informe seu numero!.'
                },
                phone: {
                  country: 'BR',
                  message: 'Informe um telefone válido.'
                }
              },
            },
            assunto: {
              validators: {
                notEmpty: {

                }
              }
            }
          }
        });
      });
      </script>
