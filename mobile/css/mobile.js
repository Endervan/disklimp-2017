import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "*": {
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "textDecoration": "none"
    },
    "body": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none",
        "fontWeight": "400",
        "fontFamily": "'Lato', sans-serif !important",
        "textAlign": "justify"
    },
    "img": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "p": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none",
        "fontSize": 17,
        "color": "#000",
        "textAlign": "justify",
        "fontWeight": "300"
    },
    "h1": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "h2": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "h3": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "h4": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "h5": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "h6": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "ul": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "ol": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "li": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none"
    },
    "select": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none",
        "minHeight": 42,
        "color": "#000",
        "borderRadius": "0 !important",
        "fontSize": 15,
        "fontWeight": "300"
    },
    "input": {
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "listStyle": "none",
        "outline": "none",
        "border": "none",
        "minHeight": 42,
        "color": "#000",
        "borderRadius": "0 !important",
        "fontSize": 15,
        "fontWeight": "300"
    },
    "top5": {
        "marginTop": 5
    },
    "top7": {
        "marginTop": 7
    },
    "top10": {
        "marginTop": 10
    },
    "top15": {
        "marginTop": 15
    },
    "top20": {
        "marginTop": 20
    },
    "top25": {
        "marginTop": 25
    },
    "top30": {
        "marginTop": 30
    },
    "top35": {
        "marginTop": 35
    },
    "top40": {
        "marginTop": 40
    },
    "top45": {
        "marginTop": 45
    },
    "top50": {
        "marginTop": 50
    },
    "top55": {
        "marginTop": 55
    },
    "top60": {
        "marginTop": 60
    },
    "top65": {
        "marginTop": 65
    },
    "top70": {
        "marginTop": 70
    },
    "top75": {
        "marginTop": 75
    },
    "top80": {
        "marginTop": 80
    },
    "top85": {
        "marginTop": 85
    },
    "top90": {
        "marginTop": 90
    },
    "top95": {
        "marginTop": 95
    },
    "top100": {
        "marginTop": 100
    },
    "top105": {
        "marginTop": 105
    },
    "top120": {
        "marginTop": 120
    },
    "top140": {
        "marginTop": 140
    },
    "top150": {
        "marginTop": 150
    },
    "top170": {
        "marginTop": 170
    },
    "top175": {
        "marginTop": 175
    },
    "top180": {
        "marginTop": "180px !important"
    },
    "top195": {
        "marginTop": 195
    },
    "top210": {
        "marginTop": "210px !important"
    },
    "top220": {
        "marginTop": "220px !important"
    },
    "top425": {
        "marginTop": "425px !important"
    },
    "bottom5": {
        "marginBottom": 5
    },
    "bottom10": {
        "marginBottom": 10
    },
    "bottom15": {
        "marginBottom": 15
    },
    "bottom20": {
        "marginBottom": 20
    },
    "bottom25": {
        "marginBottom": 25
    },
    "bottom30": {
        "marginBottom": 30
    },
    "bottom35": {
        "marginBottom": 35
    },
    "bottom40": {
        "marginBottom": 40
    },
    "bottom45": {
        "marginBottom": 45
    },
    "bottom50": {
        "marginBottom": 50
    },
    "bottom55": {
        "marginBottom": 55
    },
    "bottom60": {
        "marginBottom": 60
    },
    "bottom65": {
        "marginBottom": 65
    },
    "bottom70": {
        "marginBottom": 70
    },
    "bottom75": {
        "marginBottom": 75
    },
    "bottom80": {
        "marginBottom": 80
    },
    "bottom100": {
        "marginBottom": 100
    },
    "bottom120": {
        "marginBottom": 120
    },
    "bottom135": {
        "marginBottom": 135
    },
    "left5": {
        "marginLeft": 5
    },
    "left10": {
        "marginLeft": 10
    },
    "left15": {
        "marginLeft": 15
    },
    "left20": {
        "marginLeft": 20
    },
    "left25": {
        "marginLeft": 25
    },
    "left30": {
        "marginLeft": 30
    },
    "left35": {
        "marginLeft": 35
    },
    "left40": {
        "marginLeft": 40
    },
    "left45": {
        "marginLeft": 45
    },
    "left50": {
        "marginLeft": 50
    },
    "left55": {
        "marginLeft": 55
    },
    "left60": {
        "marginLeft": 60
    },
    "left65": {
        "marginLeft": 65
    },
    "left70": {
        "marginLeft": 70
    },
    "left75": {
        "marginLeft": 75
    },
    "left80": {
        "marginLeft": 80
    },
    "left130": {
        "marginLeft": 130
    },
    "right5": {
        "marginRight": 5
    },
    "right10": {
        "marginRight": 10
    },
    "right15": {
        "marginRight": 15
    },
    "right20": {
        "marginRight": 20
    },
    "right25": {
        "marginRight": 25
    },
    "right30": {
        "marginRight": 30
    },
    "right35": {
        "marginRight": 35
    },
    "right40": {
        "marginRight": 40
    },
    "right45": {
        "marginRight": 45
    },
    "right50": {
        "marginRight": 50
    },
    "right55": {
        "marginRight": 55
    },
    "right60": {
        "marginRight": 60
    },
    "right65": {
        "marginRight": 65
    },
    "right70": {
        "marginRight": 70
    },
    "right75": {
        "marginRight": 75
    },
    "right80": {
        "marginRight": 80
    },
    "right130": {
        "marginRight": 130
    },
    "pt10": {
        "paddingTop": 10
    },
    "pt15": {
        "paddingTop": 15
    },
    "pt20": {
        "paddingTop": 20
    },
    "pt30": {
        "paddingTop": 30
    },
    "pt40": {
        "paddingTop": 40
    },
    "pt45": {
        "paddingTop": 45
    },
    "pt50": {
        "paddingTop": 50
    },
    "pt60": {
        "paddingTop": 60
    },
    "pt80": {
        "paddingTop": 80
    },
    "pt100": {
        "paddingTop": 100
    },
    "pt170": {
        "paddingTop": 170
    },
    "pt300": {
        "paddingTop": 300
    },
    "pl10": {
        "paddingLeft": 10
    },
    "pl15": {
        "paddingLeft": 15
    },
    "pl20": {
        "paddingLeft": 20
    },
    "pl30": {
        "paddingLeft": 30
    },
    "pl40": {
        "paddingLeft": 40
    },
    "pl45": {
        "paddingLeft": 45
    },
    "pl50": {
        "paddingLeft": 50
    },
    "pl60": {
        "paddingLeft": 60
    },
    "pl80": {
        "paddingLeft": 80
    },
    "pl100": {
        "paddingLeft": 100
    },
    "pl300": {
        "paddingLeft": 300
    },
    "pr10": {
        "paddingRight": 10
    },
    "pr15": {
        "paddingRight": 15
    },
    "pr20": {
        "paddingRight": 20
    },
    "pr30": {
        "paddingRight": 30
    },
    "pr40": {
        "paddingRight": 40
    },
    "pr45": {
        "paddingRight": 45
    },
    "pr50": {
        "paddingRight": 50
    },
    "pr60": {
        "paddingRight": 60
    },
    "pr80": {
        "paddingRight": 80
    },
    "pr100": {
        "paddingRight": 100
    },
    "pr300": {
        "paddingRight": 300
    },
    "pb10": {
        "paddingBottom": 10
    },
    "pb15": {
        "paddingBottom": 15
    },
    "pb20": {
        "paddingBottom": 20
    },
    "pb25": {
        "paddingBottom": 25
    },
    "pb30": {
        "paddingBottom": 30
    },
    "pb40": {
        "paddingBottom": "40px !important"
    },
    "pb50": {
        "paddingBottom": 50
    },
    "pb60": {
        "paddingBottom": 60
    },
    "pb70": {
        "paddingBottom": 70
    },
    "pb80": {
        "paddingBottom": 80
    },
    "pb100": {
        "paddingBottom": 100
    },
    "pb280": {
        "paddingBottom": 280
    },
    "pb300": {
        "paddingBottom": 300
    },
    "container": {
        "maxWidth": 480
    },
    "input100": {
        "width": "100% !important"
    },
    "pg10": {
        "paddingTop": 10,
        "paddingRight": "!important",
        "paddingBottom": 10,
        "paddingLeft": "!important"
    },
    "h1 span": {},
    "h3 span": {},
    "h5 span": {},
    "h6 span": {},
    "p span": {
        "fontSize": 18
    },
    "font-futura": {
        "fontFamily": "futura__condensed"
    },
    "lato_bold": {
        "fontFamily": "Lato Bold"
    },
    "MyriadPro_Regular": {
        "fontFamily": "MyriadPro-Regular"
    },
    "lato_black": {
        "fontFamily": "Lato Black"
    },
    "a:hover": {
        "textDecoration": "none !important"
    },
    "media-left": {
        "verticalAlign": "middle !important"
    },
    "media-right": {
        "verticalAlign": "middle !important"
    },
    "media-body": {
        "verticalAlign": "middle !important"
    },
    "relativo": {
        "position": "relative"
    },
    "textarea": {
        "minHeight": 42,
        "color": "#000",
        "borderRadius": "0 !important",
        "fontSize": 15,
        "fontWeight": "300"
    },
    "::-webkit-input-placeholder": {
        "color": "#000",
        "fontFamily": "Lato Bold",
        "fontSize": 20
    },
    ":-moz-placeholder": {
        "color": "#000",
        "fontFamily": "Lato Bold",
        "fontSize": 20
    },
    "::-moz-placeholder": {
        "color": "#000",
        "fontFamily": "Lato Bold",
        "fontSize": 20
    },
    ":-ms-input-placeholder": {
        "color": "#000",
        "fontFamily": "Lato Bold",
        "fontSize": 20
    },
    "btn": {
        "borderRadius": 0
    },
    "padding0": {
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "topo-site": {
        "position": "absolute",
        "top": 0,
        "zIndex": 1,
        "width": "100%"
    },
    "topo": {
        "height": 60,
        "background": "#ed3237",
        "position": "relative",
        "zIndex": 999
    },
    "btn_carrinho": {
        "background": "#fff !important",
        "WebkitAppearance": "inherit !important"
    },
    "carrinho_servcos": {
        "width": 40,
        "height": 30
    },
    "carrinho_servcos_orcamento": {
        "width": 62,
        "height": 50
    },
    "servicos_dentros": {
        "marginBottom": 20,
        "width": 60,
        "height": 60
    },
    "icon_telefone h2": {
        "fontSize": 20,
        "fontFamily": "Lato Bold",
        "color": "#000"
    },
    "icon_telefone img": {
        "height": 40
    },
    "lista-menu a": {
        "display": "block",
        "textAlign": "left",
        "color": "#000"
    },
    "btn_transparente_topo": {
        "background": "url(../imgs/topo_menu_hover.png)",
        "color": "#fff",
        "fontFamily": "futura__condensed",
        "fontSize": 17,
        "paddingTop": 2,
        "paddingRight": 10,
        "paddingBottom": 2,
        "paddingLeft": 10
    },
    "btn_transparente_topo:hover": {
        "background": "url(../imgs/topo_menu_hover.png)",
        "color": "#fff",
        "fontFamily": "futura__condensed",
        "fontSize": 17,
        "paddingTop": 2,
        "paddingRight": 10,
        "paddingBottom": 2,
        "paddingLeft": 10
    },
    "btn_transparente_contatos": {
        "background": "#ed3237",
        "color": "#fff",
        "fontFamily": "futura__condensed",
        "fontSize": 17,
        "paddingTop": 2,
        "paddingRight": 10,
        "paddingBottom": 2,
        "paddingLeft": 10
    },
    "btn_transparente_contatos:hover": {
        "background": "#ed3237",
        "color": "#fff",
        "fontFamily": "futura__condensed",
        "fontSize": 17,
        "paddingTop": 2,
        "paddingRight": 10,
        "paddingBottom": 2,
        "paddingLeft": 10
    },
    "topo h2": {
        "fontFamily": "Lato Black",
        "fontSize": 15,
        "color": "#000"
    },
    "topo media-left": {
        "paddingRight": 2
    },
    "topo media>pull-left": {
        "paddingRight": 2
    },
    "btn_localizacao": {
        "background": "#ffff01 !important",
        "color": "#000",
        "fontWeight": "400",
        "fontSize": "12px !important",
        "paddingTop": 6,
        "paddingRight": 16,
        "paddingBottom": 6,
        "paddingLeft": 16
    },
    "topo-meu-orcamento": {
        "background": "#fff",
        "width": "470px !important",
        "marginRight": -4,
        "paddingTop": 15,
        "paddingRight": 15,
        "paddingBottom": 15,
        "paddingLeft": 15
    },
    "lista-itens-carrinho": {
        "marginTop": 10,
        "marginRight": 0,
        "marginBottom": 20,
        "marginLeft": 0,
        "overflow": "hidden",
        "borderBottom": "2px solid #e5e5e5"
    },
    "lista-itens-carrinho h1": {
        "fontSize": "20px !important"
    },
    "menu a": {
        "fontSize": 18,
        "color": "#000"
    },
    "carroucel-home carousel-caption": {
        "textAlign": "left",
        "zIndex": 10,
        "bottom": "inherit",
        "top": 200,
        "right": 0,
        "left": 20
    },
    "carroucel-home carousel-inner > item > img": {
        "position": "absolute",
        "top": 0,
        "left": 0,
        "width": "100%",
        "height": 451
    },
    "carroucel-home": {
        "position": "relative",
        "marginTop": 62
    },
    "carroucel-home carousel-indicators": {
        "position": "absolute",
        "top": 405,
        "left": "37%",
        "zIndex": 20,
        "width": "24%"
    },
    "carroucel-home li": {
        "opacity": 0.5,
        "width": 20,
        "height": 20,
        "background": "#fff",
        "border": 0,
        "marginTop": "3px!important",
        "marginRight": "3px!important",
        "marginBottom": "3px!important",
        "marginLeft": "3px!important"
    },
    "carroucel-home liactive": {
        "opacity": 1,
        "width": 20,
        "height": 20,
        "background": "#fff",
        "border": 0,
        "marginTop": "3px!important",
        "marginRight": "3px!important",
        "marginBottom": "3px!important",
        "marginLeft": "3px!important"
    },
    "carroucel-home carousel-control": {
        "opacity": 1,
        "color": "#fff",
        "fontSize": 40,
        "top": 402,
        "width": 20,
        "zIndex": 999
    },
    "carroucel-home spanfa-angle-right": {
        "right": 312,
        "fontSize": 30,
        "color": "#fff",
        "position": "absolute"
    },
    "carroucel-home spanfa-angle-left": {
        "left": 16,
        "fontSize": 30,
        "color": "#fff",
        "position": "absolute"
    },
    "carroucel-home carousel-control:hover": {
        "color": "#fff",
        "opacity": 0.5
    },
    "carroucel-home carousel": {
        "height": 451
    },
    "carroucel-home carousel item": {
        "height": 451
    },
    "carroucel-home carousel-caption h1": {
        "fontSize": 20,
        "color": "#fff",
        "fontFamily": "Lato Black",
        "textShadow": "2px 1px 2px rgba(0, 0, 0, 1)"
    },
    "carousel-example-generic-clientes carousel-indicators": {
        "bottom": -50
    },
    "slider-parceiros-clientes carousel-indicators": {
        "bottom": -50
    },
    "carousel-example-generic-clientes carousel-indicators li": {
        "width": 0,
        "height": 0,
        "background": "#7f7f7f",
        "border": "none",
        "marginRight": "5px!important"
    },
    "slider-parceiros-clientes carousel-indicators li": {
        "width": 0,
        "height": 0,
        "background": "#7f7f7f",
        "border": "none",
        "marginRight": "5px!important"
    },
    "carousel-example-generic-empresa carousel-indicators li": {
        "width": 0,
        "height": 0,
        "background": "#7f7f7f",
        "border": "none",
        "marginRight": "5px!important"
    },
    "slider-parceiros-empresa carousel-indicators li": {
        "width": 0,
        "height": 0,
        "background": "#7f7f7f",
        "border": "none",
        "marginRight": "5px!important"
    },
    "carousel-example-generic-clientes carousel-indicators active": {
        "background": "#000",
        "marginTop": 1,
        "marginRight": 1,
        "marginBottom": 1,
        "marginLeft": 1
    },
    "slider-parceiros-clientes carousel-indicators active": {
        "background": "#000",
        "marginTop": 1,
        "marginRight": 1,
        "marginBottom": 1,
        "marginLeft": 1
    },
    "carousel-example-generic-empresa carousel-indicators active": {
        "background": "#000",
        "marginTop": 1,
        "marginRight": 1,
        "marginBottom": 1,
        "marginLeft": 1
    },
    "slider-parceiros-empresa carousel-indicators active": {
        "background": "#000",
        "marginTop": 1,
        "marginRight": 1,
        "marginBottom": 1,
        "marginLeft": 1
    },
    "carousel-example-generic-clientes carousel-controlleft": {
        "background": "none",
        "left": 133,
        "top": "inherit",
        "bottom": -47,
        "width": 25,
        "height": 25,
        "zIndex": 100
    },
    "carousel-example-generic-clientes carousel-controlright": {
        "background": "none",
        "right": 133,
        "top": "inherit",
        "bottom": -61,
        "width": 40,
        "height": 40,
        "zIndex": 100
    },
    "slider-parceiros-clientes carousel-controlleft": {
        "background": "none",
        "left": 133,
        "top": "inherit",
        "bottom": -47,
        "width": 25,
        "height": 25,
        "zIndex": 100
    },
    "slider-parceiros-clientes carousel-controlright": {
        "background": "none",
        "right": 133,
        "top": "inherit",
        "bottom": -61,
        "width": 40,
        "height": 40,
        "zIndex": 100
    },
    "carousel-example-generic-empresa carousel-controlleft": {
        "background": "none"
    },
    "carousel-example-generic-empresa carousel-controlright": {
        "background": "none"
    },
    "slider-parceiros-empresa carousel-controlleft": {
        "background": "none"
    },
    "slider-parceiros-empresa carousel-controlright": {
        "background": "none"
    },
    "carousel-example-generic-clientes fa-chevron-left": {
        "fontSize": 0,
        "color": "#000"
    },
    "carousel-example-generic-clientes fa-chevron-right": {
        "fontSize": 0,
        "color": "#000"
    },
    "slider-parceiros-clientes fa-chevron-left": {
        "fontSize": 0,
        "color": "#000"
    },
    "slider-parceiros-clientes fa-chevron-right": {
        "fontSize": 0,
        "color": "#000"
    },
    "carousel-example-generic-empresa fa-chevron-left": {
        "fontSize": 0,
        "color": "#000"
    },
    "carousel-example-generic-empresa fa-chevron-right": {
        "fontSize": 0,
        "color": "#000"
    },
    "slider-parceiros-empresa fa-chevron-left": {
        "fontSize": 0,
        "color": "#000"
    },
    "slider-parceiros-empresa fa-chevron-right": {
        "fontSize": 0,
        "color": "#000"
    },
    "div_personalizada": {
        "paddingTop": "0px !important",
        "paddingBottom": "0px !important"
    },
    "bg-fundo-preto p": {
        "color": "#fff",
        "fontWeight": "700",
        "paddingTop": 10,
        "paddingRight": 0,
        "paddingBottom": 10,
        "paddingLeft": 0,
        "textAlign": "center"
    },
    "bg-fundo-preto": {
        "position": "relative",
        "background": "#000"
    },
    "icon-rodape": {
        "color": "#fff"
    },
    "orcamento_geral h1": {
        "fontSize": 41,
        "color": "#fff",
        "fontWeight": "400",
        "textShadow": "1px 2px 5px #0f0f0f"
    },
    "orcamento_geral h1 span": {
        "fontSize": 44,
        "color": "#fff",
        "fontWeight": "400",
        "textShadow": "1px 2px 5px #0f0f0f",
        "marginTop": 5
    },
    "tabela_carrinho thead th": {
        "background": "#d41f2b",
        "paddingTop": 10,
        "paddingRight": 5,
        "paddingBottom": 10,
        "paddingLeft": 5
    },
    "tabela_carrinho table": {
        "fontSize": 15,
        "color": "#000",
        "fontWeight": "400"
    },
    "tabela_carrinho table>tbody>tr>td": {
        "verticalAlign": "middle",
        "borderBottom": "1px solid #ffff01",
        "paddingTop": 10,
        "paddingRight": 5,
        "paddingBottom": 10,
        "paddingLeft": 5
    },
    "tabela_carrinho table>thead>tr>th": {
        "verticalAlign": "middle",
        "borderBottom": "1px solid #ffff01",
        "paddingTop": 10,
        "paddingRight": 5,
        "paddingBottom": 10,
        "paddingLeft": 5
    },
    "fa-times-circle::before": {
        "color": "#ffff01",
        "textShadow": "1px 2px 5px #0f0f0f"
    },
    "input-lista-prod-orcamentos": {
        "width": 32,
        "minHeight": "36px !important",
        "textAlign": "center",
        "background": "#ffff01",
        "color": "#000",
        "WebkitBorderRadius": 5,
        "MozBorderRadius": 5,
        "borderRadius": 5,
        "fontSize": 13
    },
    "tabela_carrinho h5 span": {
        "fontSize": 20,
        "color": "#000",
        "fontFamily": "Lato Bold"
    },
    "fundo_formulario h5 span": {
        "fontSize": 20,
        "color": "#000",
        "fontFamily": "Lato Bold"
    },
    "tabela_carrinho h3": {
        "color": "#000",
        "fontSize": 25,
        "fontFamily": "Lato Bold"
    },
    "fundo_formulario h3": {
        "color": "#000",
        "fontSize": 25,
        "fontFamily": "Lato Bold"
    },
    "fundo-form": {
        "background": "#eaeaea"
    },
    "image_orcamento img": {
        "width": "59px !important",
        "height": "50px !important"
    },
    "pinBoot": {
        "position": "relative",
        "maxWidth": "100%",
        "width": "100%"
    },
    "pintrest iframe": {
        "width": "100%",
        "maxWidth": "100%",
        "height": "auto"
    },
    "pintrest img": {
        "width": "100%",
        "maxWidth": "100%",
        "height": "auto"
    },
    "white-panel": {
        "position": "absolute",
        "background": "white",
        "boxShadow": "0px 1px 2px rgba(0, 0, 0, 0.3)",
        "paddingTop": 10,
        "paddingRight": 10,
        "paddingBottom": 10,
        "paddingLeft": 10
    },
    "white-panel:hover": {
        "boxShadow": "1px 1px 10px rgba(0, 0, 0, 0.5)",
        "marginTop": -5,
        "WebkitTransition": "all 0.3s ease-in-out",
        "MozTransition": "all 0.3s ease-in-out",
        "OTransition": "all 0.3s ease-in-out",
        "transition": "all 0.3s ease-in-out"
    },
    "contaos_loja": {
        "marginTop": 0
    },
    "trabalhe_geral h1": {
        "fontSize": 25,
        "color": "#fff",
        "fontWeight": "400",
        "textShadow": "1px 2px 5px #0f0f0f"
    },
    "trabalhe_geral h1 span": {
        "fontSize": 28,
        "color": "#fff",
        "fontWeight": "400",
        "textShadow": "1px 2px 5px #0f0f0f",
        "marginTop": 12
    },
    "btn_formulario": {
        "borderRadius": 0,
        "fontSize": 20,
        "color": "#fff",
        "paddingTop": 15,
        "paddingRight": 25,
        "paddingBottom": 15,
        "paddingLeft": 25,
        "background": "#ed3237"
    },
    "btn_formulario:hover": {
        "color": "#fff",
        "background": "#bc302f",
        "opacity": 1
    },
    "btn_formulario1": {
        "borderRadius": 0,
        "fontSize": 20,
        "color": "#000",
        "paddingTop": 15,
        "paddingRight": 25,
        "paddingBottom": 15,
        "paddingLeft": 25,
        "background": "#ffff01"
    },
    "btn_formulario1:hover": {
        "color": "#000",
        "background": "#dfd619",
        "opacity": 0.9
    },
    "sombra_fale": {
        "zIndex": 99999,
        "position": "absolute",
        "bottom": 2,
        "right": 0,
        "height": 28,
        "background": "url(../imgs/sombra_lojas.png) center no-repeat",
        "width": "100%"
    },
    "contatos-btn-opcoes aactive": {
        "background": "#ff0000"
    },
    "contatos-btn-opcoes a": {
        "fontSize": 20,
        "color": "#fff",
        "background": "#f6989b",
        "display": "block",
        "paddingTop": 15,
        "paddingBottom": 15,
        "marginBottom": 10
    },
    "contatos-btn-opcoes a span": {
        "fontSize": 21,
        "fontWeight": "400",
        "textDecoration": "none"
    },
    "contato_dados h1": {
        "marginTop": 10,
        "marginBottom": 0
    },
    "contato_dados h1 span": {
        "fontSize": 25
    },
    "dicas_Dentro": {
        "background": "#eaeaea",
        "position": "relative",
        "overflow": "hidden"
    }
});