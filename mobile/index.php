<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>


  <!-- ======================================================================= -->
  <!-- topo	-->
  <!-- ======================================================================= -->

  <?php require_once('./includes/topo.php'); ?>

  <!-- ======================================================================= -->
  <!-- topo	-->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 limit 3");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">

                <?php
                if ($imagem[url] == '') {
                  ?>
                  <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                  <?php
                }else{
                  ?>
                  <a href="<?php Util::imprime($imagem[url]) ?>">
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                  </a>
                  <?php
                }
                ?>


              </div>

              <?php
              $i++;
            }
          }
          ?>


        </div>



        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="fa fa-angle-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="fa fa-angle-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>



      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- BOLETO,CARTAO,SERVICOS,ATENTMENTO    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-6">
        <div class="fundo_cinza">
          <div class="media  ">
            <div class="media-left media-middle">

              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_boleto.png" alt="">

            </div>
            <div class="media-body  pb5 pt5">
              <h2 class="media-heading ">10% OFF <span class="clearfix">NO BOLETO</span></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="fundo_cinza">
          <div class="media ">
            <div class="media-left media-middle">

              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_cartao.png" alt="">

            </div>
            <div class="media-body pb5 pt5">
              <h2 class="media-heading">EM ATE 5X <span class="clearfix">NO CARTÃO</span></h2>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BOLETO,CARTAO,SERVICOS,ATENTMENTO    -->
  <!-- ======================================================================= -->

  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row top55">
      <div class="col-xs-12">
        <div class="lista-produtos-index">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs bottom25" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">EM DESTAQUES</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">OS MAIS VENDIDOS</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">OS MAIS VISITADOS</a></li>
            <!-- <li role="presentation"><a href="<?php// echo Util::caminho_projeto() ?>/produtos" class="btn btn-preto">VER TODOS OS PRODUTOS</a></li> -->
            <div>



            </div>
          </ul>
        </div>
      </div>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">

          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->


        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home01.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->

        </div>


        <div role="tabpanel" class="tab-pane" id="messages">
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home02.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->

        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->




  <!-- ======================================================================= -->
  <!-- SERVICOS HOME -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50">


      <div class="col-xs-12 dicas_titulo">
        <h1 class="pb20">NOSSAS <span class="clearfix">NOTÍCIAS</span></h1>
      </div>

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- nossas noticias -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">

    <?php
    $result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 2");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-6 top10 dicas_geral">
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="caption">
              <h1 class="dicas_desc"><span><?php Util::imprime($row[titulo],80); ?></span></h1>
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" class="btn leia_mais_dicas top10" role="button">
                LEIA MAIS <i class="fa fa-long-arrow-right left10" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>


    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- nossas noticias -->
  <!-- ======================================================================= -->

  <div class="bottom20">  </div>



  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/modal.php') ?>
  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- modal    -->
  <!-- ======================================================================= -->

</body>

</html>





<?php require_once('./includes/js_css.php') ?>
