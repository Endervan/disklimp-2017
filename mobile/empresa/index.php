
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 165px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12 breadcrumbs_preto">
				<div class="breadcrumb top10">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
					<a class="active">A EMPRESA</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->


	<div class="container">
		<div class="row ">

			<!-- ======================================================================= -->
			<!-- conheca nossa empresa  -->
			<!-- ======================================================================= -->
			<div class="col-xs-8 empresa_geral">
				<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
				<div class="top70 text-left">
					<h3>CONHEÇA NOSSA<span> HISTÓRIA</span></h3>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="col-xs-12 top150 desc_empresa_missao">
				<p><?php Util::imprime($row[descricao]); ?></p>
			</div>
			<!-- ======================================================================= -->
			<!-- conheca nossa empresa  -->
			<!-- ======================================================================= -->


			<div class="col-xs-12 empresa_titulo1 top40">
				<!-- ======================================================================= -->
				<!-- ATENTIMENTO  -->
				<!-- ======================================================================= -->
				<div class="">
					<h3><span>ATENTIMENTO</span></h3>
				</div>
				<!-- ======================================================================= -->
				<!-- ATENTIMENTO  -->
				<!-- ======================================================================= -->

				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="top35 endereco_empresa">
					<div class="telefone_rodape">

						<p class="col-xs-6 padding0">
							<img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone.png" alt="" class="right10">
							<a href="tel+:<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>" title="ligue">
								<span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?>
							</a>
						</p>

						<?php if (!empty($config[telefone2])) { ?>
							<p class="col-xs-6 padding0">
								<img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone.png" alt="" class="right10">
								<a href="tel+:<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>" title="ligue">
									<span><?php Util::imprime($config[ddd2]); ?> </span> <?php Util::imprime($config[telefone2]); ?>
								</a>
							</p>
							<?php } ?>

							<?php if (!empty($config[telefone3])) { ?>
								<p class="col-xs-6 padding0">
									<img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone.png" alt="" class="right10">
									<a href="tel+:<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>" title="ligue">
										<span><?php Util::imprime($config[ddd3]); ?> </span> <?php Util::imprime($config[telefone3]); ?>
									</a>
								</p>
								<?php } ?>

								<?php if (!empty($config[telefone4])) { ?>
									<p class="col-xs-6 padding0">
										<img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_telefone.png" alt="" class="right10">
										<a href="tel+:<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>" title="ligue">
											<span><?php Util::imprime($config[ddd4]); ?> </span> <?php Util::imprime($config[telefone4]); ?>
										</a>
									</p>
									<?php } ?>
								</div>

								<div class="clearfix"></div>

								<div class="top30">
									<h3><span>ONDE ESTAMOS</span></h3>
								</div>


								<div class="media top35">
									<div class="media-left media-middle">
										<img class="" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

									</div>
									<div class="media-body ">
										<p class="media-heading right60"><?php Util::imprime($config[endereco]); ?></p>
									</div>
								</div>



							</div>
							<!-- ======================================================================= -->
							<!-- ENDERECO E TELEFONES    -->
							<!-- ======================================================================= -->
						</div>
					</div>
				</div>


				<div class='container '>
					<div class="row">

						<div class="col-xs-12 top30 bottom30">
							<!-- ======================================================================= -->
							<!-- mapa   -->
							<!-- ======================================================================= -->
							<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

							<!-- ======================================================================= -->
							<!-- mapa   -->
							<!-- ======================================================================= -->
						</div>
					</div>
				</div>

				<!-- ======================================================================= -->
				<!-- rodape    -->
				<!-- ======================================================================= -->
				<?php require_once('../includes/rodape.php') ?>
				<!-- ======================================================================= -->
				<!-- rodape    -->
				<!-- ======================================================================= -->


			</body>

			</html>

			<?php require_once("../includes/js_css.php"); ?>

			<!-- ======================================================================= -->
			<!-- modal    -->
			<!-- ======================================================================= -->
			<?php require_once('../includes/modal.php') ?>
			<!-- ======================================================================= -->
			<!-- modal    -->
			<!-- ======================================================================= -->
