
<div class="container">
  <div class="row bg-topo">

    <div class="col-xs-5 logo top10 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile">
        <img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
      </a>
    </div>

    <div class="col-xs-7 top20">


      <div class="media pull-right">
        <div class="media-left media-middle">
          <h2 class="media-heading">
            <?php Util::imprime($config[ddd1]) ?><?php Util::imprime($config[telefone1]) ?>
          </h2>
        </div>
        <div class="media-body">
          <a href="tel:+55<?php Util::imprime($config[ddd1]) ?> <?php Util::imprime($config[telefone1]) ?>" class="btn btn_transparente_topo">CHAMAR</a>
        </div>
      </div>

      <div class="pull-right">

        <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-menu">
          <img class="top10" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_menu.jpg" alt="">
        </a>




      </div>


    </div>

    <div class="col-xs-12 padding0 bg_roxo top10">

      <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">

        <!--  ==============================================================  -->
        <!-- BARRA CATEGORIAS E BTN PROCURA-->
        <!--  ==============================================================  -->
        <div class="col-xs-6 padding0">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-lista-categorias">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_categorias.png" alt="">
            </a>
        </div>

        <div class="col-xs-1 padding0 busca">
          <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-encontre-o-produto-ideal">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_search.png" alt="">
          </a>
        </div>
        <!--  ==============================================================  -->
        <!-- BARRA CATEGORIAS E BTN PROCURA-->
        <!--  ==============================================================  -->
      </form>

      <div class="col-xs-5">
      <div class="pull-right">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-topo-itens-orcamento">
          <img class="" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_itens_orcamento.jpg" alt="">
        </a>
      </div>

      </div>

    </div>
  </div>


</div>


</div>
