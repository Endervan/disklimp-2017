
<!-- ======================================================================= -->
<!-- MODAL CARRINHO  -->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal-topo-itens-orcamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Itens do Meu Orçamento</h4>
      </div>
      <div class="modal-body">

        <!--  ==============================================================  -->
        <!--sessao adicionar produtos-->
        <!--  ==============================================================  -->
        <?php
        if(count($_SESSION[solicitacoes_produtos]) > 0)
        {
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
          {
            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <div class="lista-itens-carrinho">
              <div class="col-xs-2">
                <?php if(!empty($row[imagem])): ?>
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                <?php endif; ?>
              </div>
              <div class="col-xs-8">
                <p><?php Util::imprime($row[titulo]) ?></p>
              </div>
              <div class="col-xs-1">
                <a class="btn" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
              </div>
              <div class="col-xs-12"><div class="borda_carrinho top5"></div></div>
            </div>
            <div class="clearfix"></div>
            <?php
          }
        }
        ?>


        <!--  ==============================================================  -->
        <!--sessao adicionar servicos-->
        <!--  ==============================================================  -->
        <?php
        if(count($_SESSION[solicitacoes_servicos]) > 0)
        {
          for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
          {
            $row = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
            <div class="lista-itens-carrinho">
              <div class="col-xs-2">
                <?php if(!empty($row[imagem])): ?>
                  <img class="carrinho_servcos" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
                  <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
                <?php endif; ?>
              </div>
              <div class="col-xs-8">
                <p><?php Util::imprime($row[titulo]) ?></p>
              </div>
              <div class="col-xs-1">
                <a class="btn" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
              </div>
              <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

            </div>
            <div class="clearfix"></div>
            <?php
          }
        }
        ?>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" class="btn btn-primary">Finalizar Orçamento</a>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL CARRINHO  -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- MODAL MENU  -->
<!-- ======================================================================= -->
<div class="modal fade menu" id="myModal-menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Menu</h4>
      </div>
      <div class="modal-body lista-menu text-left">

        <div class="list-group">
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile">HOME</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/noticias">NOTÍCIAS</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVICOS</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</a>
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL MENU  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- MODAL CATEGORIAS PRODUTO	-->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal-lista-categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Selecione a Categoria desejada</h4>
      </div>
      <div class="modal-body">


        <div class="list-group">
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/">
            Ver Todos
          </a>

          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                <?php Util::imprime($row[titulo]); ?>
              </a>
              <?php
            }
          }
          ?>
        </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL CATEGORIAS PRODUTO	-->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- CAEGORIAS E SUB-CATEGORIAS  -->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal_categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nossas Categorias</h4>
      </div>
      <div class="modal-body">

        <?php
        $result = $obj_site->select("tb_categorias_produtos");
        if (mysql_num_rows($result) > 0) {
          while($row = mysql_fetch_array($result)){
            ++$i;
            ?>
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item"><?php Util::imprime($row[titulo]); ?></a>


            <?php
            $result1 = $obj_site->select("tb_subcategorias_produtos", "and id_categoriaproduto = $row[0] ");
            if (mysql_num_rows($result1) > 0) {
              while($row1 = mysql_fetch_array($result1)){
                ?>
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>/<?php Util::imprime($row1[url_amigavel]); ?>" class="list-group-item">
                  <i class="fa fa-angle-double-right left10" aria-hidden="true"></i>
                  <?php Util::imprime($row1[titulo]); ?>
                </a>
                <?php
              }
            }
            ?>

            <?php
          }
        }
        ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- CAEGORIAS E SUB-CATEGORIAS  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- MODAL MARCAS	-->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">NOSSAS MARCAS</h4>
      </div>
      <div class="modal-body">


        <div class="list-group">
          <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/">
            Ver Todos
          </a>

          <?php
          $result = $obj_site->select("tb_tipos_veiculos");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              ?>
              <a class="list-group-item" href="<?php echo Util::caminho_projeto() ?>/mobile/marca/<?php Util::imprime($row[url_amigavel]); ?>">
                <?php Util::imprime($row[titulo]); ?>
              </a>
              <?php
            }
          }
          ?>
        </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL MARCAS	-->
<!-- ======================================================================= -->






<!-- ======================================================================= -->
<!-- MODAL ENCONTRE O PRODUTO IDEAL -->
<!-- ======================================================================= -->
<div class="modal fade menu" id="myModal-encontre-o-produto-ideal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><h4>ENCONTRE O PRODUTOS IDEAL:</h4></h4>
      </div>
      <div class="modal-body text-left">
        <!--  ==============================================================  -->
        <!--BARRA PESQUISAS-->
        <!--  ==============================================================  -->

        <div class="col-xs-12">
          <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
            <div class="input-group stylish-input-group">

              <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" >

              <span class="input-group-addon">
                <button type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </form>

        </div>
        <!--  ==============================================================  -->
        <!--BARRA PESQUISAS-->
        <!--  ==============================================================  -->



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default top20" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- MODAL ENCONTRE O PRODUTO IDEAL -->
<!-- ======================================================================= -->




<?php  /*
<!-- ======================================================================= -->
<!-- MODAL LOJAS endereco-->
<!-- ======================================================================= -->
<div class="modal fade menu" id="myModal-lojas-topo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel">NOSSAS LOJAS</h4>
</div>
<div class="modal-body nossa_lojas_empresa">


<?php
//$result = $obj_site->select("tb_categorias_unidades","ORDER BY titulo");
if (mysql_num_rows($result) > 0) {
while($row = mysql_fetch_array($result)){
++$i;
?>
<h5><span><?php Util::imprime($row[titulo]); ?></span></h5>
<img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.jpg" alt="">

<?php
//$result1 = $obj_site->select("tb_unidades", "and id_categoriaunidade = $row[0] ");
if (mysql_num_rows($result1) > 0) {
while($row1 = mysql_fetch_array($result1)){
?>

<div class="media">
<div class="media-left media-middle">
<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="" />
</div>
<div class="media-body">
<!-- ======================================================================= -->
<!-- CONTATOS   -->
<!-- ======================================================================= -->
<div class="col-xs-12 ">
<h1 class="media-heading"><?php Util::imprime($row1[endereco]); ?></h1>
</div>

<div class="col-xs-7">

<h3  class="pull-left top10">
<a href="tel:+55<?php Util::imprime($row1[ddd1]); ?><?php Util::imprime($row1[telefone1]); ?>">
<?php Util::imprime($row1[ddd1]) ?></span><?php Util::imprime($row1[telefone1]) ?>
</a>
</h3>

<?php if (!empty($row1[telefone2])): ?>
<h3 class="pull-right top10">
<a href="tel:+55<?php Util::imprime($row1[ddd2]); ?><?php Util::imprime($row1[telefone2]); ?>">
<span><?php Util::imprime($row1[ddd2]) ?></span><?php Util::imprime($row1[telefone2]) ?><i class="fa fa-whatsapp left5" aria-hidden="true"></i>
</a>
</h3>
<?php endif; ?>

<?php if (!empty($row1[telefone3])): ?>
<a href="tel:+55<?php Util::imprime($row1[ddd3]); ?><?php Util::imprime($row1[telefone3]); ?>">
<h3 class="pull-left top10">
<span><?php Util::imprime($row1[ddd3]) ?></span><?php Util::imprime($row1[telefone3]) ?>
</h3>
</a>
<?php endif; ?>

<?php if (!empty($row1[telefone4])): ?>
<a href="tel:+55<?php Util::imprime($row1[ddd4]); ?><?php Util::imprime($row1[telefone4]); ?>">
<h3 class="pull-right top10">
<span><?php Util::imprime($row1[ddd4]) ?></span><?php Util::imprime($row1[telefone4]) ?>
</h3>
</a>
<?php endif; ?>


</div>

<div class="col-xs-4">
<a href="<?php echo Util::caminho_projeto() ?>/mobile/loja/<?php Util::imprime($row1[url_amigavel]); ?>" class="btn btn_localizacao">SAIBA MAIS</a>
</div>
<!-- ======================================================================= -->
<!-- CONTATOS   -->
<!-- ======================================================================= -->
</div>

</div>
<?php
}
}
?>

<?php
}
}
?>


</div>
<div class="modal-footer">
<button type="button" class="btn btn-default top20" data-dismiss="modal">Fechar</button>
</div>
</div>
</div>
</div>
<!-- ======================================================================= -->
<!-- MODAL LOJA endereco-->
<!-- ======================================================================= -->
*/ ?>
