
<div class="container bg-fundo-preto top50">
	<div class="row ">
		<div class=" col-xs-8 text-right">
			<p>© Copyright DISKLIMP</p>
		</div>
		<div class="col-xs-4">
			<a href="http://www.homewebbrasil.com.br" class="pull-right top5" target="_blank" >
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-homeweb.png" alt="" height="40">
			</a>

			<?php if ($config[facebook] != "") { ?>
			<a href="<?php Util::imprime($config[facebook]); ?>" title="Google Plus" target="_blank" >
				<i class="fa fa-facebook right15 top15 icon-rodape"></i>
			</a>
			<?php } ?>

			<?php if ($config[twitter] != "") { ?>
			<a href="<?php Util::imprime($config[twitter]); ?>" title="Google Plus" target="_blank" >
				<i class="fa fa-twitter right15 top15 icon-rodape"></i>
			</a>
			<?php } ?>

			<?php if ($config[instagram] != "") { ?>
			<a href="<?php Util::imprime($config[instagram]); ?>" title="Google Plus" target="_blank" >
				<i class="fa fa-instagram right15 top15 icon-rodape"></i>
			</a>
			<?php } ?>

			<?php if ($config[google_plus] != "") { ?>
				<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" class="pull-right right20">
					<p><i class="fa fa-google-plus icon-rodape"></i></p>
				</a>
				<?php } ?>

			</div>
		</div>

	</div>
