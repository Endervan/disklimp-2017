<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];


if(!empty($url))
{
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
	Util::script_location(Util::caminho_projeto()."/mobile/noticias/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 165px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12 breadcrumbs_branco">
				<div class="breadcrumb top10">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/noticias/" data-toggle="tooltip" data-placement="top" title="noticias">NOTÍCIAS</i></a>

					<!-- <a class="active">NOTÍCIA</a> -->
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!--NOTICIAS DENTRO-->
	<!-- ======================================================================= -->
	<div class='container'>
		<div class="row top20">

			<div class="col-xs-12  ">
				<div class="noticias_geral">
					<h1 class="pb20 ">CONFIRA NOSSAS <span>  NOTÍCIAS</span></h1>
				</div>
			</div>

			<div class="col-xs-12 top20 noticia_Dentro">

				<div class="text-center bottom10">
					<h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
				</div>
				<?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",775, 316, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>
				<div class="top25">
					<p><?php Util::imprime($dados_dentro[descricao]); ?></p>
				</div>
			</div>

		</div>
	</div>
	<!-- ======================================================================= -->
	<!--NOTICIAS DENTRO-->
	<!-- ======================================================================= -->

	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>

<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
