
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 165px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12 breadcrumbs_preto">
				<div class="breadcrumb top30">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
					<a class="active">SERVIÇOS</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->




	<!-- ======================================================================= -->
	<!-- SERVICOS TITULO  -->
	<!-- ======================================================================= -->
	<div class="container top30">
		<div class="row">
			<div class="col-xs-12 servicos_geral">
				<h3>CONHEÇA NOSSOS<span class="clearfix"> SERVIÇOS</span></h3>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- SERVICOS TITULO  -->
	<!-- ======================================================================= -->





	<!-- ======================================================================= -->
	<!-- SERVICOS HOME -->
	<!-- ======================================================================= -->
	<div class="container ">
		<div class="row top150">

				<?php
				$result = $obj_site->select("tb_servicos");
				if (mysql_num_rows($result) > 0) {
					while($row = mysql_fetch_array($result)){
						//  busca a qtd de produtos cadastrados
						// $result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


						switch ($i) {
							case 1:
							$cor_class = 'BALANCEAMENTO';
							break;
							case 2:
							$cor_class = 'RODIZIO';
							break;
							case 3:
							$cor_class = 'SUSPENSÃO';
							break;
							case 4:
							$cor_class = 'REPARO';
							break;
							case 5:
							$cor_class = 'ALIANHAMENTO';
							break;

							default:
							$cor_class = 'CALIBRAGEM';
							break;
						}
						$i++;
						?>


						<!-- ======================================================================= -->
						<!--ITEM 01 REPETI ATE 6 ITENS   -->
						<!-- ======================================================================= -->
						<div class="col-xs-12 servicos pt20 pb20 <?php echo $cor_class; ?> ">

							<div class="media text-left">
								<a class="media-left" href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
								
									<div class="media-body">

										<h2 class="media-heading servico_tiulo"><?php Util::imprime($row[titulo]); ?></h2>
										<div class=" servicos_desc">
											<p><?php Util::imprime($row[descricao],103); ?></p>
										</div>
									</a>
								</div>

							</div>


						</div>

						<?php
					}
				}
				?>


		</div>
	</div>
</div>
<!-- ======================================================================= -->
<!-- SERVICOS HOME -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>

<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
