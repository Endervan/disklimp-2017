
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 165px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->

	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row">
			<div class="col-xs-10 breadcrumbs_preto">
				<div class="breadcrumb top10">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
					<a class="active">FALE CONOSCO</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->



	<!-- ======================================================================= -->
	<!-- fale conosco -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top20 ">
			<div class="col-xs-8 ">
				<div class="">
					<h4>FAÇA PARTE DO NOSSO TIME<span>  TRABALHE CONOSCO</span></h4>
				</div>
			</div>


			<div class="clearfix">	</div>
			<!-- ======================================================================= -->
			<!-- ENDERECO E TELEFONES    -->
			<!-- ======================================================================= -->
			<div class="top35 endereco_empresa">
				<div class="telefone_rodape">

					<p class="col-xs-6">
						<a href="tel+:<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>" title="ligue">
							<span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?>
						</a>
					</p>

					<div class="clearfix">	</div>

					<?php if (!empty($config[telefone2])) { ?>
						<p class="col-xs-6">
							<a href="tel+:<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>" title="ligue">
								<span><?php Util::imprime($config[ddd2]); ?> </span> <?php Util::imprime($config[telefone2]); ?>
							</a>
						</p>
						<?php } ?>

						<?php if (!empty($config[telefone3])) { ?>
							<p class="col-xs-6">
								<a href="tel+:<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>" title="ligue">
									<span><?php Util::imprime($config[ddd3]); ?> </span> <?php Util::imprime($config[telefone3]); ?>
								</a>
							</p>
							<?php } ?>

							<?php if (!empty($config[telefone4])) { ?>
								<p class="col-xs-6">
									<a href="tel+:<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>" title="ligue">
										<span><?php Util::imprime($config[ddd4]); ?> </span> <?php Util::imprime($config[telefone4]); ?>
									</a>
								</p>
								<?php } ?>
							</div>

							<div class="clearfix">	</div>
							<div class="col-xs-12">
								<div class=" empresa_geral top80">
									<h3>ENVIE SEU <span> CURRÍCULO</span></h3>
								</div>

							</div>
						</div>
					</div>
					<!-- ======================================================================= -->
					<!-- ENDERECO E TELEFONES    -->
					<!-- ======================================================================= -->


					<!--  ==============================================================  -->
					<!-- FORMULARIO-->
					<!--  ==============================================================  -->
					<div class="col-xs-12">
						<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
							<div class="fundo_formulario">
								<!-- formulario orcamento -->

									<div class="col-xs-12 padding0 top20">
										<div class="form-group  input100 has-feedback">
											<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
											<span class="fa fa-user form-control-feedback top15"></span>
										</div>
									</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group  input100 has-feedback">
											<input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
											<span class="fa fa-envelope form-control-feedback top15"></span>
										</div>
									</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
											<span class="glyphicon glyphicon-phone-alt form-control-feedback top5"></span>
										</div>
									</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="text" name="sexo" class="form-control fundo-form1 input100  input-lg" placeholder="SEXO">
											<span class="fa fa-star form-control-feedback"></span>
										</div>
									</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback">
											<input type="date" id="data" name="nascimento" class="form-control fundo-form1 input100  input-lg" placeholder="NASCIMENTO" >
											<span class="fa fa-calendar form-control-feedback"></span>
										</div>
									</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="text" name="grau" class="form-control fundo-form1 input100  input-lg" placeholder="GRAU DE INSTRUÇÃO">
											<span class="glyphicon glyphicon-list-alt form-control-feedback"></span>
										</div>
									</div>


									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="text" name="endereco" class="form-control fundo-form1 input100  input-lg" placeholder="ENDEREÇO">
											<span class="glyphicon glyphicon-map-marker form-control-feedback top5"></span>
										</div>
									</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="ÁREA PRETENTIDA">
											<span class="fa fa-folder-open form-control-feedback top5"></span>
										</div>
									</div>




								<div class="top20">
									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO PRETENTIDA">
											<span class="fa fa-folder-open form-control-feedback top5"></span>
										</div>
									</div>
									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback ">
											<input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
											<span class="fa fa-file-text form-control-feedback top15"></span>
										</div>
									</div>
								</div>

									<div class="col-xs-12 padding0 top20">
										<div class="form-group input100 has-feedback">
											<textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
											<span class="fa fa-pencil form-control-feedback top15"></span>
										</div>
									</div>




								<div class="col-xs-12 top20 text-right">
									<div class="top15 bottom25">
										<button type="submit" class="btn btn_formulario1" name="btn_contato">
											ENVIAR
										</button>
									</div>
								</div>

							</div>
						</form>
					</div>
					<!--  ==============================================================  -->
					<!-- FORMULARIO-->
					<!--  ==============================================================  -->
					<div class="clearfix"></div>

					<div class="col-xs-12">
						<div class=" empresa_geral_barra top30">
							<h3><span>ONDE ESTAMOS</span></h3>
						</div>


						<div class="media top35">
							<div class="media-left media-middle">
								<img class="right20" src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_localizacao.png" alt="" />

							</div>
							<div class="media-body ">
								<p class="media-heading right60"><?php Util::imprime($config[endereco]); ?></p>
							</div>
						</div>
					</div>


				</div>
			</div>

			<!-- ======================================================================= -->
			<!-- fale conosco -->
			<!-- ======================================================================= -->


			<div class='container '>
				<div class="row">

					<div class="col-xs-12 padding0 top20">
						<!-- ======================================================================= -->
						<!-- mapa   -->
						<!-- ======================================================================= -->
						<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="401" frameborder="0" style="border:0" allowfullscreen></iframe>

						<!-- ======================================================================= -->
						<!-- mapa   -->
						<!-- ======================================================================= -->
					</div>
				</div>
			</div>

			<!-- ======================================================================= -->
			<!-- rodape    -->
			<!-- ======================================================================= -->
			<?php require_once('../includes/rodape.php') ?>
			<!-- ======================================================================= -->
			<!-- rodape    -->
			<!-- ======================================================================= -->


		</body>

		</html>

		<?php require_once("../includes/js_css.php"); ?>

		<!-- ======================================================================= -->
		<!-- modal    -->
		<!-- ======================================================================= -->
		<?php require_once('../includes/modal.php') ?>
		<!-- ======================================================================= -->
		<!-- modal    -->
		<!-- ======================================================================= -->

		      <?php
		      //  VERIFICO SE E PARA ENVIAR O EMAIL
		      if(isset($_POST[nome]))
													      {

													        if(!empty($_FILES[curriculo][name])):
													          $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
													          $texto = "Anexo: ";
													          $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
													          $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
													        endif;

													        $texto_mensagem = "
													        Nome: ".$_POST[nome]." <br />
													        Email: ".$_POST[email]." <br />
													        Telefone: ".$_POST[telefone]." <br />
													        Sexo: ".$_POST[sexo]." <br />
													        Nascimento: ".$_POST[nascimento]." <br />
													        Grau de Instrução: ".$_POST[grau]." <br />
													        Endereço: ".$_POST[endereco]." <br />
													        Area Pretentida: ".$_POST[area]." <br />
													        Cargo: ".$_POST[cargo]." <br />

													        Mensagem: <br />
													        ".nl2br($_POST[mensagem])."

													        <br><br>
													        $texto
													        ";


													        if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
													          Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
													          Util::alert_bootstrap("Obrigado por entrar em contato.");
													          unset($_POST);
													        }else{
													          Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
													        }

		    								}

		      ?>



		<script>
		$(document).ready(function() {
			$('.FormContatos').bootstrapValidator({
				message: 'This value is not valid',
				feedbackIcons: {
					valid: 'fa fa-check',
					invalid: 'fa fa-remove',
					validating: 'fa fa-refresh'
				},
				fields: {
					nome: {
						validators: {
							notEmpty: {
								message: 'Informe nome.'
							}
						}
					},
					email: {
						validators: {
							notEmpty: {
								message: 'insira email.'
							},
							emailAddress: {
								message: 'Esse endereço de email não é válido'
							}
						}
					},
					telefone: {
						validators: {
							notEmpty: {
								message: 'Por favor adicione seu numero.'
							},
							phone: {
								country: 'BR',
								message: 'Informe um telefone válido.'
							}
						},
					},
					assunto: {
						validators: {
							notEmpty: {

							}
						}
					},
					endereco1: {
						validators: {
							notEmpty: {

							}
						}
					},
					cidade: {
						validators: {
							notEmpty: {
								message: 'Por favor adicione sua Cidade.'
							}
						}
					},
					estado: {
						validators: {
							notEmpty: {
								message: 'Por favor adicione seu Estado.'
							}
						}
					},
					escolaridade1: {
						validators: {
							notEmpty: {
								message: 'Sua Informação Acadêmica.'
							}
						}
					},
					curriculo: {
						validators: {
							notEmpty: {
								message: 'Por favor insira seu currículo'
							},
							file: {
								extension: 'doc,docx,pdf,rtf',
								type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
								maxSize: 5*1024*1024,   // 5 MB
								message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
							}
						}
					},
					mensagem: {
						validators: {
							notEmpty: {
								message: 'Adicione sua Mensagem.'
							}
						}
					}
				}
			});
		});
		</script>


		<!-- ======================================================================= -->
		<!-- MOMENTS  -->
		<!-- ======================================================================= -->
		<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
		<script type="text/javascript">

		$('#hora').datetimepicker({
			format: 'LT'
		});

		</script>
