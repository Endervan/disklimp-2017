

<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 165px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12 breadcrumbs_branco">
				<div class="breadcrumb top10">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
					<a class="active">PRODUTOS</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- barra categoria    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12  bottom25 top20">
				<div class="noticias_geral">
					<h1>CONHEÇA NOSSOS <span>  PRODUTOS</span></h1>
				</div>
			</div>

			<div class="col-xs-12">
				<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal-lista-categorias">
					<img class="top10" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/cat_produtos.png" alt="">
				</a>
			</div>


		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- barra categoria    -->
	<!-- ======================================================================= -->



	<!-- ======================================================================= -->
	<!-- nossas PRODUTOS -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row">

			<?php
			$get = explode("/", $_GET[get1]);


			 //  FILTRA PELA MARCA
			 $url1 = $get[0];
			 $filtro = $get[1];



			 if($url1 != ''){
				 $id_categoria = $obj_site->get_id_url_amigavel("tb_tipos_veiculos", "idtipoveiculo", $url1 );
				 if($id_categoria == ''){
						 header("location: ../produtos");
				 }
				 $complemento .= "AND id_tipoveiculo = '$id_categoria' ";
			 }

			 $result = $obj_site->select("tb_produtos", $complemento);

			 ?>


			<div class="col-xs-6 top30 total-resultado-busca top15">
				<h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S).</h1>
			</div>

			<div class="col-xs-6 btn_busca_marcas bottom25 top20">
					<!--  ==============================================================  -->
					<!-- ORDENAR POR MARCAS-->
					<!--  ==============================================================  -->
					<a class="col-xs-12 padding0" href="javascript:void(0);" data-toggle="modal" data-target="#myModal_marcas" title="MARCAS">
						MARCAS <i class="fa fa-angle-down pull-right" aria-hidden="true"></i>
					</a>
					<!--  ==============================================================  -->
					<!-- ORDENAR POR MARCAS-->
					<!--  ==============================================================  -->
			</div>


			<?php
     			 if(mysql_num_rows($result) == 0){
	               echo "<div class='clearfix'></div>
	               <h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
	             }else{
					 require_once("../includes/lista_produtos.php");
				 }
			?>


		</div>
	</div>

	<!-- ======================================================================= -->
	<!-- nossas PRODUTOS -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>

<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
<?php require_once('../includes/modal.php') ?>
<!-- ======================================================================= -->
<!-- modal    -->
<!-- ======================================================================= -->
