<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];


if(!empty($url))
{
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
	Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 165px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12 breadcrumbs_branco">
				<div class="breadcrumb top10">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/" data-toggle="tooltip" data-placement="top" title="HOME"><i class="fa fa-home"></i></a>
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos/" data-toggle="tooltip" data-placement="top" title="produto">PRODUTOS</i></a>

					<!-- <a class="active">NOTÍCIA</a> -->
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->


	<div class="container">
		<div class="row top20 produtos_dentro">


			<!-- ======================================================================= -->
			<!-- SLIDER CATEGORIA -->
			<!-- ======================================================================= -->
			<div class="col-xs-10 col-xs-offset-1 slider_prod_geral">

				<!-- Place somewhere in the <body> of your page -->
				<div id="slider" class="flexslider">
					<ul class="slides slider-prod">
						<?php
						$result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
						if(mysql_num_rows($result) > 0)
						{
							while($row = mysql_fetch_array($result)){
								?>
								<li class="zoom">
									<a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4" title="<?php Util::imprime($dados_dentro[titulo]); ?>">
										<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" />
									</a>
								</li>
								<?php
							}
						}
						?>

						<!-- items mirrored twice, total of 12 -->
					</ul>


				</div>
				<div id="carousel" class="flexslider">
					<i class="fa fa-search" aria-hidden="true">  Click na imagem para ver o zoom</i>

					<ul class="slides slider-prod-tumb">
						<?php
						$result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
						if(mysql_num_rows($result) > 0)
						{
							while($row = mysql_fetch_array($result)){
								?>
								<li>
									<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />

								</li>
								<?php
							}
						}
						?>
						<!-- items mirrored twice, total of 12 -->
					</ul>
				</div>


			</div>
			<!-- ======================================================================= -->
			<!-- SLIDER CATEGORIA -->
			<!-- ======================================================================= -->


			<!-- ======================================================================= -->
			<!-- DESCRICAO TIPOS PRODUTOS  -->
			<!-- ======================================================================= -->
			<div class="col-xs-12 top25 padding0 descricao_tipo_produto">

				<div class="col-xs-12">
					<h1 class="text-uppercase"><?php Util::imprime($dados_dentro[titulo]); ?></h1>

					<div class="clearfix"> </div>

					<?php /*
					<div class="top10">
					<h3 class="preco"><span>R$ </span> <?php echo Util::formata_moeda($dados_dentro[preco]); ?></h3>
					</div>
					*/ ?>

					<div class="top15">
						<a href="javascript:void(0);" class="" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
							<img  src="<?php echo Util::caminho_projeto() ?>/imgs/btn_produto_dentro.png"/>
						</a>
					</div>
				</div>

				<div class=" col-xs-12 padding0">
					<div class="top20 col-xs-8">
						<a class=" btn_ligue" >
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/central.png" alt="">
						</a>
					</div>

					<!-- ======================================================================= -->
					<!-- ENDERECO E TELEFONES    -->
					<!-- ======================================================================= -->
					<div class="col-xs-12 top25 telefones">
						<div class="telefone_rodape">

							<p class="col-xs-6">
								<a href="tel+:<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>" title="ligue">
									<span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?>
								</a>
							</p>


							<?php if (!empty($config[telefone2])) { ?>
								<p class="col-xs-6">
									<a href="tel+:<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>" title="ligue">
										<span><?php Util::imprime($config[ddd2]); ?> </span> <?php Util::imprime($config[telefone2]); ?>
									</a>
								</p>
								<?php } ?>

								<?php if (!empty($config[telefone3])) { ?>
									<p class="col-xs-6">
										<a href="tel+:<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>" title="ligue">
											<span><?php Util::imprime($config[ddd3]); ?> </span> <?php Util::imprime($config[telefone3]); ?>
										</a>
									</p>
									<?php } ?>

									<?php if (!empty($config[telefone4])) { ?>
										<p class="col-xs-6">
											<a href="tel+:<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>" title="ligue">
												<span><?php Util::imprime($config[ddd4]); ?> </span> <?php Util::imprime($config[telefone4]); ?>
											</a>
										</p>
										<?php } ?>
									</div>
									<!-- ======================================================================= -->
									<!-- ENDERECO E TELEFONES    -->
									<!-- ======================================================================= -->
								</div>


								<div class="col-xs-12 descricao_prod top10">
									<h2><i class="btn_prod">MARCA :</i> <span class="left10"><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_tipoveiculo], "tb_tipos_veiculos", "idtipoveiculo", "titulo") ); ?></span></h2>


								</div>

							</div>
							<!-- ======================================================================= -->
							<!-- DESCRICAO TIPOS PRODUTOS  -->
							<!-- ======================================================================= -->

						</div>
					</div>
				</div>




				<div class="container">
					<div class="row produtos_dentro_desc">
						<!-- ======================================================================= -->
						<!-- DESCRICAO PRODUTOS  -->
						<!-- ======================================================================= -->
						<div class="col-xs-12 bottom20 top20 barra_produto ">
							<h3 class="pb20"> <span>DESCRIÇÃO </span></h3>
						</div>

						<div class="col-xs-12 top10 bottom40">
							<p><?php Util::imprime($dados_dentro[descricao]); ?></p>

						</div>
						<!-- ======================================================================= -->
						<!-- DESCRICAO PRODUTOS  -->
						<!-- ======================================================================= -->


					</div>
				</div>



				<!--  ==============================================================  -->
				<!--  VEJA TAMBEM-->
				<!--  ==============================================================  -->
				<div class="container">
					<div class="row">

						<div class="col-xs-12 bottom20 barra_produto ">
							<h3 class="pb20"> <span>VEJA TAMBÉM </span></h3>
						</div>

						<?php
		        $result = $obj_site->select("tb_produtos", " order by rand() limit 2");
		        if(mysql_num_rows($result) == 0){
		          echo "<div class='clearfix'></div>
		          <h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
		        }else{

		          ?>

		          <?php

		          while ($row = mysql_fetch_array($result))
		          {
		            $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
		            $row_categoria = mysql_fetch_array($result_categoria);
		            ?>

								<div class="col-xs-6">
									<div class="lista-produto">
										<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
											<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 270, 260, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
										</a>

										<?php /*
										<div class="text-right right10">
										<h3 class="preco"><span>R$ </span> <?php echo Util::formata_moeda($row[preco]); ?></h3>
										</div>
										*/ ?>

										<h2 class="text-uppercase"><?php Util::imprime($row_categoria[titulo]); ?></h2>
										<h1 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h1>




										<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="pull-left btn-carrinho-saiba-mais">SAIBA MAIS</a>
										<a href="javascript:void(0);" title="Adicionar ao orçamento"  data-toggle="tooltip" data-placement="top" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" class="pull-right btn-vermelho btn-carrinho">
											<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_orcamento.png" alt="">
										</a>
										<div class="clearfix"></div>
									</div>
								</div>
								<?php if ($i == 4) {
									echo'<div class="clearfix"></div>';
									$i = 0;
								}else{
									$i++;
								}
							}
						}
						?>

					</div>
				</div>
				<!--  ==============================================================  -->
				<!--  VEJA TAMBEM-->
				<!--  ==============================================================  -->


				<!-- ======================================================================= -->
				<!-- rodape    -->
				<!-- ======================================================================= -->
				<?php require_once('../includes/rodape.php') ?>
				<!-- ======================================================================= -->
				<!-- rodape    -->
				<!-- ======================================================================= -->


			</body>

			</html>

			<?php require_once("../includes/js_css.php"); ?>


			<!-- ======================================================================= -->
			<!-- modal    -->
			<!-- ======================================================================= -->
			<?php require_once('../includes/modal.php') ?>
			<!-- ======================================================================= -->
			<!-- modal    -->
			<!-- ======================================================================= -->


			<script>
			$(window).load(function() {
				// The slider being synced must be initialized first
				$('#carousel').flexslider({
					animation: "slide",
					controlNav: false,
					animationLoop: false,
					slideshow: false,
					itemWidth: 100,
					itemMargin: 10,
					asNavFor: '#slider'
				});

				$('#slider').flexslider({
					animation: "slide",
					controlNav: false,
					animationLoop: false,
					slideshow: false,
					sync: "#carousel"
				});
			});


			</script>
