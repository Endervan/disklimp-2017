<?php
require_once('./class/Util.class.php');

$assunto = 'Teste de envio localhost';
$mensagem = 'Teste de envio localhost de email';
$nomeremetente = 'João -' . rand( 0, 100000);
$emailremetente = 'joao@masmidia.com.br';

Util::envia_email('marcio@masmidia.com.br', $assunto, $mensagem, $nomeremetente, $emailremetente);
Util::envia_email('suporte@masmidia.com.br', $assunto, $mensagem, $nomeremetente, $emailremetente);
Util::envia_email('terezinhadedonno@gmail.com', $assunto, $mensagem, $nomeremetente, $emailremetente);

/*
// If you are using Composer (recommended)
require 'vendor/autoload.php';

// If you are not using Composer
// require("path/to/sendgrid-php/sendgrid-php.php");

$from = new SendGrid\Email("Example User", "mas.informatica@gmail.com");
$subject = "Sending with SendGrid is Fun";
$to = new SendGrid\Email("Example User", "marciomas@gmail.com");
$content = new SendGrid\Content("text/plain", "and easy to do anywhere, even with PHP");
$mail = new SendGrid\Mail($from, $subject, $to, $content);

$apiKey = ('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');
$sg = new \SendGrid($apiKey);

$response = $sg->client->mail()->send()->post($mail);
echo $response->statusCode();
print_r($response->headers());
echo $response->body();
*/
